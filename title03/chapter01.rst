################################
CAPÍTULO I DEL DISTRITO NOTARIAL
################################

Artículo 127.- Definición
#########################

Se considera distrito notarial a la demarcación territorial de la República en la que ejerce competencia un colegio de notarios.

Artículo 128.- Número de Distritos Notariales
#############################################

Los Distritos Notariales de la República son veintidós con la demarcación territorial establecida.
