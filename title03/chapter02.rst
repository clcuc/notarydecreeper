#######################################
CAPÍTULO II DE LOS COLEGIOS DE NOTARIOS
#######################################

Artículo 129.- Definición
#########################

Los colegios de notarios son personas jurídicas de derecho público, cuyo funcionamiento se rige por Estatuto Único.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 129.- Definición

Los colegios de notarios son personas jurídicas de derecho público, cuyo funcionamiento se rige por Estatuto, que deberá ceñirse a la presente Ley y su Reglamento.”

Artículo 130.- Atribuciones y Obligaciones
##########################################

Corresponde a los colegios de notarios:

a) La vigilancia directa del cumplimiento por parte del notario de las leyes y reglamentos que regulen la función.

b) Velar por el decoro profesional, el cumplimiento del Código de Ética del notariado y acatamiento de la presente Ley, normas reglamentarias y conexas así como el estatuto del colegio.

c) El ejercicio de la representación gremial de la orden.

d) Promover la eficacia de los servicios notariales y la mejora del nivel profesional de sus miembros.

e) Llevar registro actualizado de sus miembros, el mismo que incluye la información establecida en el artículo 14, así como los principales datos del notario y su oficio notarial y de las licencias concedidas, así como cualquier otra información, que disponga el Consejo del Notariado. Los datos contenidos en este registro podrán ser total o parcialmente publicados por medios telemáticos, para efectos de información a la ciudadanía.

f) Convocar a concurso público para la provisión de vacantes en el ámbito de su demarcación territorial y cuando lo determine el Consejo del Notariado, conforme a lo previsto en la presente ley.

CONCORDANCIAS:      D.S.Nº 015-2008-JUS, Art. 5

g) Emitir los lineamientos, así como verificar y establecer los estándares para una infraestructura mínima tanto física como tecnológica de los oficios notariales. Asimismo generar una interconexión telemática que permita crear una red notarial a nivel nacional y faculte la interconexión entre notarios, entre estos y sus colegios notariales así como entre los Colegios y la Junta de Decanos de los Colegio de Notarios del Perú.(*)

(*) De conformidad con la Cuarta Disposición Complementaria, Transitoria y Final del Decreto Supremo Nº 003-2009-JUS, publicado el 05 marzo 2009, la interconexión telemática a que alude el presente inciso, se implementará de manera gradual. En una primera etapa corresponderá a la interconexión entre notarios y su correspondiente colegio. En una segunda etapa se efectuará la interconexión entre los colegios de notarios entre sí, y finalmente entre éstos y la Junta de Decanos de los Colegios de Notarios del Perú. A efectos de lo anterior, los notarios están obligados a brindar todas las facilidades necesarias la interconexión. Posteriormente de conformidad con el Expediente N° 2450-2010, publicado el 08 septiembre 2012, se declara NULO, ILEGAL E INCONSTITUCIONAL en su totalidad el Decreto Supremo Nº 003-2009-JUS, que aprobó el Reglamento del Decreto Legislativo Nº 1049.

h) Absolver las consultas y emitir informes que le sean solicitados por los Poderes Públicos, así como absolver las consultas que le sean formuladas por sus miembros.

i) Establecer el régimen de visitas de inspecciones ordinarias anuales y extraordinarias opinadas e inopinadas de los oficios notariales de su demarcación territorial, siendo responsable de su ejecución y estricto cumplimiento.

j) Autorizar las vacaciones y licencias de sus miembros.

k) Autorizar, en cada caso, el traslado de un notario a una provincia del mismo distrito notarial, con el objeto de autorizar instrumentos, por vacancia o ausencia de notario.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 13

I) Supervisar que sus miembros mantengan las calidades señaladas en el artículo 10 de la presente ley.

m) Aplicar, en primera instancia, las sanciones previstas en la ley.

n) Velar por la integridad de los archivos notariales conservados por los notarios en ejercicio, regulando su digitalización y conversión a micro formas digitales de conformidad con la ley de la materia, así como disponer la administración de los archivos del notario cesado, encargándose del oficio y cierre de sus registros.

CONCORDANCIAS :      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arts. 32, 34 y 38

ñ) Autorizar, regular, supervisar y registrar la expedición del diploma de idoneidad a que se refiere el inciso b) del artículo 4 del Decreto Legislativo Nº 681.

o) El cierre de los registros del notario sancionado con suspensión y la designación del notario que se encargue del oficio en tanto dure dicha sanción; y,

p) Ejercer las demás atribuciones que le señale la presente ley, Estatuto y demás normas complementarias. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 130.- Atribuciones y obligaciones :

Corresponde a los colegios de notarios:

a) Vigilar directamente el cumplimiento de las leyes y reglamentos que regulen la función notarial.

b) Velar por el decoro profesional y el cumplimiento de la presente Ley, las normas reglamentarias y conexas, el Código de Ética del Notariado y el Estatuto del Colegio.

c) Ejercer la representación gremial de la orden.

d) Promover la eficacia de los servicios notariales y la mejora del nivel profesional de sus miembros.

e) Llevar un registro actualizado de sus miembros que incluya la información prevista en el artículo 14; los principales datos del notario, de su oficio notarial y de las licencias concedidas, así como cualquier otra información que disponga el Consejo del Notariado. Los datos contenidos en este registro pueden ser total o parcialmente publicados por medios telemáticos, a efectos de brindar información a la ciudadanía. La información actualizada a la que se refiere el presente artículo, debe ser remitida al Consejo del Notariado para su incorporación al Registro Nacional de Notarios, bajo responsabilidad de los miembros de la Junta Directiva del Colegio de Notarios.

f) Convocar a concurso público para la provisión de vacantes en el ámbito de su demarcación territorial y cuando así lo determine el Consejo del Notariado, conforme a lo previsto en la presente ley.

g) Emitir los lineamientos y establecer los estándares mínimos para la infraestructura física y tecnológica de los oficios notariales.

h) Verificar el cumplimiento de los lineamientos y estándares mínimos previstos para la infraestructura física y tecnológica de los oficios notariales.

i) Generar una interconexión telemática que permita crear una red notarial a nivel nacional y faculte la interconexión entre notarios, entre estos y sus colegios notariales, así como entre dichos colegios y la Junta de Decanos de los Colegio de Notarios del Perú.

j) Absolver las consultas y emitir informes que le sean solicitados por los poderes públicos, así como absolver las consultas que le sean formuladas por sus miembros.

k) Establecer el régimen de visitas de inspección ordinarias anuales y extraordinarias respecto de los oficios notariales de su demarcación territorial.

l) Autorizar las vacaciones y licencias de sus miembros.

m) Autorizar el traslado de un notario a una provincia del mismo distrito notarial con el objeto de autorizar instrumentos, en los casos de vacancia o ausencia de notario. Si dicho traslado no se autoriza dentro del plazo de 15 días contados a partir de producida la vacancia o ausencia, el Consejo del Notariado lo dispone con conocimiento del colegio de notarios correspondiente.

n) Supervisar que sus miembros mantengan los requisitos señalados en el artículo 10 de la presente ley.

o) Aplicar las sanciones previstas en la ley.

p) Velar por la integridad de los archivos notariales conservados por los notarios en ejercicio, disponiendo su digitalización y conversión a microformas digitales de conformidad con la ley de la materia, así como disponer la administración de los archivos del notario cesado, encargándose del oficio y cierre de sus registros.

q) Autorizar, regular, supervisar y registrar la expedición del diploma de idoneidad a que se refiere el literal b) del artículo 4 del Decreto Legislativo Nº 681.

r) Cerrar los registros del notario sancionado con suspensión y designar al notario que se encargue del oficio en tanto dure dicha sanción.

s) Ejercer las demás atribuciones que le señale la presente ley, el Estatuto del Colegio y las demás normas complementarias.

t) Remitir al Consejo del Notariado, en la periodicidad y la forma que disponga la Presidencia de dicho Consejo, la información referida a las denuncias y procedimientos disciplinarios iniciados contra los miembros de su orden, en el ejercicio de la función notarial.

u) Cumplir y hacer cumplir de las disposiciones del Consejo del Notariado, bajo responsabilidad de los miembros de la Junta Directiva."

Artículo 131.- Asamblea General
###############################

La asamblea general, conformada por los miembros del colegio, es el órgano supremo del Colegio y sus atribuciones se establecen en el estatuto.

Artículo 132.- de la Junta Directiva y el Tribunal de Honor.
############################################################

El colegio de notarios será dirigido y administrado por una junta directiva, compuesta por un decano, un fiscal, un secretario y un tesorero. Podrá establecerse los cargos de vicedecano y vocales.

Asimismo, el colegio de notarios tendrá un Tribunal de Honor compuesto de tres miembros que deben ser notarios que no integren simultáneamente la junta directiva, y/o abogados de reconocido prestigio moral y profesional. El Tribunal de Honor se encargará de conocer y resolver las denuncias y procedimientos disciplinarios en primera instancia.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 132.- La Junta Directiva y el Tribunal de Honor

El colegio de notarios será dirigido y administrado por una junta directiva, compuesta por un decano, un fiscal, un secretario y un tesorero. Podrá establecerse los cargos de vicedecano y vocales.

Asimismo, el colegio de notarios tendrá un Tribunal de Honor compuesto de tres miembros que deben ser notarios que no integren simultáneamente la junta directiva, pudiendo convocar notarios de otros distritos en tanto sean elegidos por la asamblea general. El Tribunal de Honor se encargará de conocer y resolver las denuncias y procedimientos disciplinarios en primera instancia."

Artículo 133.- Elección de la Junta Directiva y Tribunal de Honor
#################################################################

Los miembros de la junta directiva son elegidos en asamblea general, mediante votación secreta, por mayoría de votos y mandato de dos años. En la misma forma y oportunidad, se elegirá a los tres miembros titulares del Tribunal de Honor, así como tres miembros suplentes que sólo actuarán en caso de abstención y/o impedimento de los titulares.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 133.- Elección de la Junta Directiva y Tribunal de Honor

Los miembros de la junta directiva son elegidos en asamblea general, mediante votación secreta, por mayoría de votos y mandato de dos años. En la misma forma y oportunidad, se elegirá a los tres miembros titulares del Tribunal de Honor, así como a los tres miembros suplentes que sólo actuarán en caso de abstención y/o impedimento de los titulares."

Artículo 134.- Ingresos de los Colegios de Notarios
###################################################

Constituyen ingresos de los colegios:

a) Las cuotas y otras contribuciones que se establezcan conforme a su Estatuto.

b) Las donaciones, legados, tributos y subvenciones que se efectúen o constituyan a su favor; y,

c) Los provenientes de la autorización y certificación de documentos, en ejercicio de las funciones establecidas según los artículos 61, 62 y 89 de la presente ley.
