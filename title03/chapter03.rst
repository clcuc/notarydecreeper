#########################################################################
CAPÍTULO III: DE LA JUNTA DE DECANOS DE LOS COLEGIOS DE NOTARIOS DEL PERU
#########################################################################

Artículo 135.- Definición
#########################

Los colegios de notarios forman un organismo denominado Junta de Decanos de los Colegios de Notarios del Perú, que coordina su acción en el orden interno y ejerce la representación del notariado en el ámbito internacional.

CONCORDANCIAS :      D.S.Nº 015-2008-JUS, Art. 7

Artículo 136.- Integrantes de la Junta de Decanos
#################################################

La Junta de Decanos de los Colegios de Notarios del Perú se integra por todos los decanos de los colegios de notarios de la República, tiene su sede en Lima, y la estructura y atribuciones que su estatuto aprobado en asamblea, determinen.(*) RECTIFICADO POR FE DE ERRATAS

Artículo 137.- El Consejo Directivo
###################################

El consejo directivo estará compuesto por un presidente, tres vicepresidentes, elegidos entre los decanos del Norte, Centro y Sur de la República, un secretario y un tesorero.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 137.- El Consejo Directivo

El Consejo Directivo estará compuesto por un presidente, tres vicepresidentes, elegidos entre los decanos del Norte, Centro y Sur de la república, un secretario y un tesorero. La presidencia del Consejo Directivo recae en el Decano del Colegio de Notarios con mayor número de agremiados” .

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 56

Artículo 138.- Fines de la Junta de Decanos
###########################################

La Junta de Decanos de los Colegios de Notarios del Perú, orientará su acción al cumplimiento de los fines institucionales, promoverá la realización de certámenes nacionales e internacionales para el estudio de disciplinas jurídicas vinculadas al notariado, a la difusión de los principios fundamentales del sistema de notariado latino, pudiendo editar publicaciones orientadas a sus fines, además de cumplir las funciones que la ley, reglamentos y su estatuto le asigne.

Artículo 139.- Ingreso de la Junta de Decanos
#############################################

Constituyen ingresos de la Junta:

a) Las cuotas y otras contribuciones que establezcan sus órganos, conforme a su estatuto.

b) Las donaciones, legados, tributos y subvenciones que se efectúen o constituyan a su favor.

c) Los ingresos por certificación de firma de notarios y otros servicios que preste de acuerdo a sus atribuciones.
