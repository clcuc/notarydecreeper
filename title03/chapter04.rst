#####################################
CAPÍTULO IV DEL CONSEJO DEL NOTARIADO
#####################################

Artículo 140.- Definición
#########################

El Consejo del Notariado es el órgano del Ministerio de Justicia que ejerce la supervisión del notariado.

Artículo 141.- Conformación del Consejo del Notariado
#####################################################

El Consejo del Notariado se integra por los siguientes miembros:

a) El Ministro de Justicia o su representante, quien lo presidirá. En caso de nombrar a su representante, éste ejercerá el cargo a tiempo completo.

b) El Fiscal de la Nación o el Fiscal Supremo o Superior, a quien delegue.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 57

c) El Decano del Colegio de Abogados de Lima o un miembro de la junta directiva a quien delegue.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 57

d) El Presidente de la Junta de Decanos de los Colegios de Notarios del Perú o un miembro del consejo directivo a quien delegue; y,

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 57

e) El Decano del Colegio de Notarios de Lima o un miembro de la junta directiva a quien delegue.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 57

El Consejo contará con el apoyo y asesoramiento de un Secretario Técnico, así como el apoyo administrativo que el Ministerio de Justicia le brinde.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arts. 56, 57, 58 y 59

Artículo 142.- Atribuciones del Consejo del Notariado
#####################################################

Son atribuciones del Consejo del Notariado:

a) Ejercer la vigilancia de los colegios de notarios respecto al cumplimiento de sus obligaciones.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, nums. 1 y 2

b) Ejercer la vigilancia de la función notarial, con arreglo a esta ley y normas reglamentarias o conexas, a través del colegio de notarios, sin perjuicio de su intervención directa cuando así lo determine.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, nums. 1 y 2

c) Proponer los reglamentos y normas para el mejor desenvolvimiento de la función notarial.

d) Aprobar directivas de cumplimiento obligatorio para el mejor desempeño de la función notarial y para el cumplimiento de las obligaciones de los colegios de notarios.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, num. 3

e) Vigilar el cumplimiento del reglamento de visitas de inspección a los oficios notariales por los colegios de notarios.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, num. 1

f) Realizar visitas de inspección opinadas e inopinadas a los oficios notariales, pudiendo designar a personas o instituciones para tal efecto.

g) Resolver en última instancia, como tribunal de apelación, sobre las decisiones de la junta directiva de los colegios de notarios relativas a la supervisión de la función notarial.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, num. 4

h) Resolver en última instancia como tribunal de apelación, sobre las decisiones del Tribunal de Honor de los colegios de notarios relativos a asuntos disciplinarios.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55 num. 4, Art. 60 y Art. 73

i) Designar al presidente del jurado de los concursos públicos de méritos para el ingreso a la función notarial conforme al artículo 11 de la presente ley;

j) Decidir la provisión de plazas notariales a que se refiere el artículo 5 de la presente ley.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 8

k) Solicitar al colegio de notarios la convocatoria a concursos públicos de méritos o convocarlos, conforme a lo previsto en la presente ley.

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Art. 5

l) Recibir quejas o denuncias sobre irregularidades en el ejercicio de la función notarial y darles el trámite que corresponda.

m) Recibir las quejas o denuncias sobre el incumplimiento de las obligaciones por parte de los integrantes de la junta directiva de los colegios de notarios, y darles el trámite correspondiente a una denuncia por incumplimiento de la función notarial.

n) Llevar un registro actualizado de las juntas directivas de los colegios de notarios y el registro nacional de notarios.

ñ) Absolver las consultas que formulen los poderes públicos, así como las juntas directivas de los colegios de notarios, relacionadas con la función notarial; y,

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 55, num. 4

o) Ejercer las demás atribuciones que señale la ley y normas reglamentarias o conexas.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 142.- Atribuciones del Consejo del Notariado

Son atribuciones del Consejo del Notariado:

a) Ejercer la vigilancia de los colegios de notarios respecto al cumplimiento de sus obligaciones.

b) Ejercer la vigilancia de la función notarial, con arreglo a esta ley y normas reglamentarias o conexas.

c) Proponer los reglamentos y normas para el mejor desenvolvimiento de la función notarial.

d) Aprobar directivas de cumplimiento obligatorio para el mejor desempeño de la función notarial y para el cumplimiento de las obligaciones de los colegios de notarios, en el ejercicio de la función notarial.

e) Vigilar el cumplimiento del reglamento de visitas de inspección a los oficios notariales por los colegios de notarios.

f) Establecer la política de inspecciones opinadas e inopinadas a los oficios notariales y colegios de notarios.

g) Resolver en última instancia, como tribunal de apelación, sobre las decisiones de la junta directiva de los colegios de notarios relativas a la supervisión de la función notarial.

h) Resolver en última instancia como tribunal de apelación, sobre las decisiones del Tribunal de Honor de los colegios de notarios relativos a asuntos disciplinarios.

i) Designar al presidente del jurado de los concursos públicos de méritos para el ingreso a la función notarial conforme al artículo 11 de la presente ley.

j) Decidir la provisión de plazas notariales a que se refiere el artículo 5 de la presente ley.

k) Solicitar al colegio de notarios la convocatoria a concursos públicos de méritos o convocarlos, conforme a lo previsto en la presente ley.

l) Recibir quejas o denuncias sobre irregularidades en el ejercicio de la función notarial y darles el trámite que corresponda.

m) Recibir las quejas o denuncias sobre el incumplimiento de las obligaciones por parte de los integrantes de la junta directiva y del Tribunal de Honor de los colegios de notarios, y darles el trámite correspondiente a una denuncia por incumplimiento de la función notarial.

n) Llevar un registro actualizado de las juntas directivas de los colegios de notarios y el registro nacional de notarios.

o) Absolver las consultas que formulen los poderes públicos, así como las juntas directivas de los colegios de notarios, relacionadas con la función notarial.

p) Supervisar la utilización del papel seriado y del papel notarial que administran los colegios de notarios.

q) Ejercer las demás atribuciones que señale la ley y normas reglamentarias o conexas” .

“ Artículo 142-A.- Atribuciones del Presidente del Consejo del Notariado

Son atribuciones del Presidente del Consejo del Notariado:

a) Dirigir el equipo técnico y administrativo en las funciones y atribuciones asignadas al Consejo del Notariado.

b) Representar al Consejo del Notariado ante los órganos competentes y en los actos públicos correspondientes.

c) Proponer al Consejo del Notariado un plan de trabajo anual respecto a la vigilancia de la función notarial, con arreglo a esta ley y normas reglamentarias o conexas.

d) Convocar al Consejo para llevar a cabo las sesiones de trabajo según corresponda.

e) Proponer ante el Consejo del Notariado los temas de agenda para las sesiones de trabajo y las mejoras institucionales para el cumplimiento de sus atribuciones.

f) Planificar, dirigir y disponer la realización de supervisiones a nivel nacional a los colegios de notarios y a los oficios notariales.

g) Planificar, dirigir y disponer la realización de inspecciones opinadas e inopinadas a los oficios notariales y a los colegios de notarios, pudiendo designar a las personas o instituciones para tal efecto.

h) Proponer normas y directivas para el mejor desarrollo de las funciones y atribuciones de la función notarial y del Consejo del Notariado” .(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

Artículo 143.- Ingresos del Consejo del Notariado
#################################################

Constituyen ingresos del Consejo del Notariado:

a) Los que generen.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 61

b) El 25% del precio de venta de papel seriado que expendan los colegios de notarios.(*)

(*) Inciso declarado inconstitucional por el Resolutivo 2 de la Sentencia de Expedientes  N° 0009, 00015 y 00029-2009-PI-TC , publicada el 30 septiembre 2010. Asimismo, conforme al Resolutivo 5 de la citada Sentencia se d ifiere sólo los efectos de la declaratoria de inconstitucionalidad del presente inciso, hasta la entrada en vigencia de la Ley de Presupuesto General de la República para el año 2012.

c) El 30 % de lo recaudado por los Colegios de Notarios de la República, por concepto de derechos que abonen los postulantes en los concursos públicos de méritos de ingreso a la función notarial.(*)

(*) Inciso declarado inconstitucional por el Resolutivo 2 de la Sentencia de Expedientes  N° 0009, 00015 y 00029-2009-PI-TC , publicada el 30 septiembre 2010. Asimismo, conforme al Resolutivo 5 de la citada Sentencia se d ifiere sólo los efectos de la declaratoria de inconstitucionalidad del presente inciso, hasta la entrada en vigencia de la Ley de Presupuesto General de la República para el año 2012.

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Art. 9

d) Las donaciones, legados y subvenciones que se efectúen o constituyan a su favor; y,

e) Los recursos que el Estado le asigne.
