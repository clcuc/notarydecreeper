===========================
Notary Decree
===========================

https://clcuc.gitlab.io/notarydecreeper

This generates a ReadTheDocs-themed website that displays a Notary Decree. A toggle switch to choose
between dark mode and light mode is also available on the website. 

Building
========
.. https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#inline-markup-and-special-characters-e-g-bold-italic-verbatim  

To build the website, ``git clone`` this repository and then, on your terminal, follow the commands
shown in the ``gitlab-ci.yml`` file. A virtual environment like ``virtualenv`` or ``conda`` is
recommended.  

Tools
=====

Some tools used in this repository:  

- Python
- Sphinx-doc with reStructuredText
- ReadTheDocs theme
- ReadTheDocs dark mode extension
