################################################################
CAPÍTULO V DE LA NULIDAD DE LOS INSTRUMENTOS PÚBLICOS NOTARIALES
################################################################

Artículo 123.- Definición
#########################

Son nulos los instrumentos públicos notariales cuando se infrinjan las disposiciones de orden público sobre la materia, contenidas en la presente ley.

“ Artículo 123-.A.- Nulidad de escrituras públicas y certificaciones de firmas

Son nulas de pleno derecho las escrituras públicas de actos de disposición o de constitución de gravamen, realizados por personas naturales sobre predios ubicados 142fuera del ámbito territorial del notario. Asimismo, la nulidad alcanza a las certificaciones de firmas realizadas por el notario, en virtud de una norma especial en los formularios o documentos privados; sin perjuicio que de oficio se instaure al notario el proceso disciplinario establecido en el Título IV de la presente ley. La presente disposición no se aplica al cónsul cuando realiza funciones notariales” .(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

(*) Artículo modificado por la Tercera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1310, publicado el 30 diciembre 2016, cuyo texto es el siguiente:

“ Artículo 123-A.- Nulidad de escrituras públicas y certificaciones de firmas

Son nulas de pleno derecho las escrituras públicas de actos de disposición o de constitución de gravamen, realizados por personas naturales sobre predios ubicados fuera del ámbito territorial del notario. Asimismo, la nulidad alcanza a las certificaciones de firmas realizadas por el notario, en virtud de una norma especial en los formularios o documentos privados; sin perjuicio de que de oficio se instaure al notario el proceso disciplinario establecido en el Título IV de la presente ley.

La presente disposición no se aplica al cónsul cuando realiza funciones notariales.

Asimismo, la restricción no alcanza a los servicios notariales que utilizan el sistema de identificación de comparación biométrica de las huellas dactilares que brinda el Registro Nacional de Identificación y Estado Civil, RENIEC. En caso de extranjeros identificados con carné de extranjería, las transacciones o actuaciones pueden realizarse ante notario de cualquier circunscripción que cuente con acceso a la base de datos de la Superintendencia Nacional de Migraciones.

Los Colegios de Notarios llevarán un registro de los notarios que cuenten con herramientas tecnológicas acreditadas para la plena identificación de las personas naturales que intervienen en los actos que se refiere el presente artículo y lo publique en su portal institucional” .

“ Artículo 123-B.- Excepciones a la nulidad prevista en el artículo 123-A

No están sujetos a la nulidad prevista en el artículo 123-A, los siguientes supuestos:

a) Actos de disposición o de constitución de gravamen mortis causa.

b) Actos de disposición o de constitución de gravamen que comprenda predios ubicados en diferentes provincias o un predio ubicado en más de una, siempre que el oficio notarial se ubique en alguna de dichas provincias.

c) Fideicomiso.

d) Arrendamiento Financiero o similar con opción de compra” .(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

Artículo 124.- Declaración de Nulidad
#####################################

La nulidad podrá ser declarada sólo por el Poder Judicial, con citación de los interesados, mediante sentencia firme.

Artículo 125.- Eficacia del Documento
#####################################

No cabe declarar la nulidad, cuando el instrumento público notarial adolece de un defecto que no afecta su eficacia documental.

Artículo 126.- Aplicación en la Declaración de Nulidad
######################################################

En todo caso, para declarar la nulidad de un instrumento público notarial, se aplicarán las disposiciones del derecho común.
