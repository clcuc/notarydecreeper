###########################################################
CAPÍTULO III DE LOS INSTRUMENTOS PÚBLICOS EXTRAPROTOCOLARES
###########################################################

SECCIÓN PRIMERA:

DISPOSICIONES GENERALES

Artículo 94.- Clases de Actas extra - protocolares
##################################################

Son actas extra - protocolares:

a) De autorización para viaje de menores.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 39

b) De destrucción de bienes.

c) De entrega.

d) De juntas, directorios, asambleas, comités y demás actuaciones corporativas.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 40

e) De licitaciones y concursos.

f) De inventarios; y subastas de conformidad con el Decreto Legislativo Nº 674, Ley de Promoción de la Inversión Privada de las Empresas del Estado.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art.41

g) De sorteos y de entrega de premios.

h) De constatación de identidad, para efectos de la prestación de servicios de certificación digital.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 42

i) De transmisión por medios electrónicos de la manifestación de voluntad de terceros; y,

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 43

j) De verificación de documentos y comunicaciones electrónicas en general.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 44

k) Otras que la ley señale

El notario llevará un índice cronológico de autorizaciones de viaje al interior y al exterior, el mismo que comunicará en la periodicidad, medios u oportunidad que señale el reglamento, a las autoridades respectivas.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Arts. 45 y 46

Artículo 95.- Clases de certificaciones
#######################################

Son certificaciones:

a) La entrega de cartas notariales.

b) La expedición de copias certificadas.

c) La certificación de firmas.

d) La certificación de reproducciones.

e) La certificación de apertura de libros.

f) La constatación de supervivencia.

g) La constatación domiciliaria; y,

h) Otras que la ley determine.

Artículo 96.- Incorporación al Protocolo
########################################

Las actas y certificaciones a que se contraen los artículos que preceden, son susceptibles de incorporarse al protocolo notarial, a solicitud de parte interesada, cumpliéndose las regulaciones que sobre el particular rigen.

Son también susceptibles de incorporarse al protocolo notarial los documentos que las partes soliciten.

Artículo 97.- Autorización de Instrumentos Extra - protocolares
###############################################################

La autorización del notario de un instrumento público extra protocolar, realizada con arreglo a las prescripciones de esta ley, da fe de la realización del acto, hecho o circunstancia, de la identidad de las personas u objetos, de la suscripción de documentos, confiriéndole fecha cierta.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 97.- Autorización de Instrumentos Públicos Extra - protocolares

La autorización del notario de un instrumento público extra protocolar, realizada con arreglo a las prescripciones de esta ley, da fe de la realización del acto, hecho o circunstancia, de la identidad de las personas u objetos, de la suscripción de documentos, confiriéndole fecha cierta.

Para garantizar la seguridad jurídica de dicho instrumento en que se efectúe la identificación de las personas, el notario podrá utilizar el sistema de comparación biométrica de huellas dactilares a través del servicio que brinda el Registro Nacional de Identidad y Estado Civil - RENIEC” .

SECCIÓN SEGUNDA:

DE LAS ACTAS EXTRAPROTOCOLARES

Artículo 98.- Definición
########################

El notario extenderá actas en las que se consigne los actos, hechos o circunstancias que presencie o le conste y que no sean de competencia de otra función.

Las actas podrán ser suscritas por los interesados y necesariamente por quien formule observación.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 47

Artículo 99.- Identificación del notario.
#########################################

Antes de la facción del acta, el notario dará a conocer su condición de tal y que ha sido solicitada su intervención para autorizar el instrumento público extraprotocolar.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 5

SECCIÓN TERCERA:

DE LA CERTIFICACIÓN DE ENTREGA DE CARTAS NOTARIALES

Artículo 100.- Definición
#########################

El notario certificará la entrega de cartas e instrumentos que los interesados le soliciten, a la dirección del destinatario, dentro de los límites de su jurisdicción, dejando constancia de su entrega o de las circunstancias de su diligenciamiento en el duplicado que devolverá a los interesados.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arst. 48 y 49

Artículo 101.- Cartas por correo certificado
############################################

El notario podrá cursar las cartas por correo certificado, a una dirección situada fuera de su jurisdicción, agregando al duplicado que devolverá a los interesados, la constancia expedida por la oficina de correo.

Artículo 102.- Responsabilidad del Contenido
############################################

El notario no asume responsabilidad sobre el contenido de la carta, ni de la firma, identidad, capacidad o representación del remitente.

Artículo 103.- Registro cronológico de Cartas
#############################################

El notario llevará un registro en el que anotará, en orden cronológico, la entrega de cartas o instrumentos notariales, el que expresará la fecha de ingreso, el nombre del remitente y del destinatario y la fecha del diligenciamiento.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 50

SECCIÓN CUARTA:

DE LA EXPEDICIÓN DE COPIAS CERTIFICADAS

Artículo 104.- Definición
#########################

El notario expedirá copia certificada que contenga la transcripción literal o parte pertinente de actas y demás documentos, con indicación, en su caso, de la certificación del libro u hojas sueltas, folios de que consta y donde obran los mismos, número de firmas y otras circunstancias que sean necesarias para dar una idea cabal de su contenido.

Artículo 105.- Responsabilidad del Contenido
############################################

El notario no asume responsabilidad por el contenido del libro u hojas sueltas, acta o documento, ni firma, identidad, capacidad o representación de quienes aparecen suscribiéndolo.

SECCIÓN QUINTA:

DE LA CERTIFICACIÓN DE FIRMAS

Artículo 106.- Definición
#########################

El notario certificará firmas en documentos privados cuando le hayan sido suscritas en su presencia o cuando le conste de modo indubitable su autenticidad.

Carece de validez la certificación de firma en cuyo texto se señale que la misma se ha efectuado por vía indirecta o por simple comparación con el documento nacional de identidad o los documentos de identidad para extranjeros.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 106.- Definición

El notario certificará firmas en documentos privados cuando hayan sido suscritos en su presencia o cuando le conste de modo indubitable la autenticidad de la firma, verificando en ambos casos la identidad de los firmantes, bajo responsabilidad.

Carece de validez la certificación de firma en cuyo texto se señale que la misma se ha efectuado por vía indirecta o por simple comparación con el documento nacional de identidad o los documentos de identidad para extranjeros” .

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 51

Artículo 107.- Testigo a Ruego
##############################

Si alguno de los otorgantes del documento no sabe o no puede firmar, lo hará una persona llevada por él a su ruego; en este caso el notario exigirá, de ser posible, la impresión de la huella digital de aquél, certificando la firma de la persona y dejando constancia, en su caso, de la impresión de la huella digital.

Artículo 108.- Responsabilidad por el Contenido
###############################################

El notario no asume responsabilidad sobre el contenido del documento de lo que deberá dejar constancia en la certificación, salvo que constituya en sí mismo un acto ilícito o contrario a la moral o a las buenas costumbres.

Artículo 109.- Documento redactado en idioma extranjero
#######################################################

El notario podrá certificar firmas en documentos redactados en idioma extranjero; en este caso, el otorgante asume la plena responsabilidad del contenido del documento y de los efectos que de él se deriven.

SECCIÓN SEXTA:

DE LA CERTIFICACIÓN DE REPRODUCCIONES

Artículo 110.- Definición
#########################

El notario certificará reproducciones de documentos obtenidos por cualquier medio idóneo, autorizando con su firma que la copia que se le presenta guarda absoluta conformidad con el original.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 52

Artículo 111.- Facultad del Notario
###################################

En caso que el documento presente enmendaduras el notario, a su criterio, podrá denegar la certificación que se le solicita o expedirla dejando constancia de la existencia de las mismas.

SECCIÓN SÉTIMA

DE LA CERTIFICACIÓN DE APERTURA DE LIBROS

Artículo 112.- Definición
#########################

El notario certifica la apertura de libros u hojas sueltas de actas, de contabilidad y otros que la ley señale.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 53

Artículo 113.- Formalidad en la Apertura de Libros
##################################################

La certificación consiste en una constancia puesta en la primera foja útil del libro o primera hoja suelta; con indicación del número que el notario le asignará; del nombre, de la denominación o razón social de la entidad; el objeto del libro; números de folios de que consta y si ésta es llevada en forma simple o doble; día y lugar en que se otorga; y, sello y firma del notario.

Todos los folios llevarán sello notarial.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 53

Artículo 114.- Registro
#######################

El notario llevará un registro cronológico de certificación de apertura de libros y hojas sueltas, con la indicación del número, nombre, objeto y fecha de la certificación.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 53

Artículo 115.- Cierre y Apertura de Libros
##########################################

Para solicitar la certificación de un segundo libro u hojas sueltas, deberá acreditarse el hecho de haberse concluido el anterior o la presentación de certificación que demuestre en forma fehaciente su pérdida.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 115.- Cierre y Apertura de Libros

Para solicitar la certificación de un segundo libro u hojas sueltas, deberá acreditarse el hecho de haberse concluido el anterior o la presentación de certificación que demuestre en forma fehaciente su pérdida.

Tratándose de la pérdida del libro de actas de una persona jurídica, se deberá presentar el acta de sesión del órgano colegiado de administración o el acta de la junta o asamblea general, en hojas simples, donde se informe de la pérdida del libro, con la certificación notarial de la firma de cada interviniente en el acuerdo, debiendo el notario verificar la autenticidad de las firmas."

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 53

Artículo 116.- Solicitud de Certificación
#########################################

La certificación a que se refiere esta sección deberá ser solicitada por el interesado o su representante, el que acreditará su calidad de tal ante el notario.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 116.- Solicitud de Certificación

La certificación a que se refiere esta sección deberá ser solicitada por:

a) La persona natural, o su apoderado o representante legal.

b) El apoderado o representante legal de la persona jurídica. En el caso de Libro de actas, matrícula de acciones y de padrón de socios, el apoderado o representante legal deberá ser identificado conforme al artículo 55 de la presente ley.”

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 53
