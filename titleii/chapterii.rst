#####################################################
CAPÍTULO II DE LOS INSTRUMENTOS PÚBLICOS PROTOCOLARES
#####################################################

Artículo 36.- Definición
########################

El protocolo notarial es la colección ordenada de registros sobre la misma materia en los que el notario extiende los instrumentos públicos protocolares con arreglo a ley.

Artículo 37.- Registros Protocolares
####################################

Forman el protocolo notarial los siguientes registros:

a) De escrituras públicas.

b) De testamentos.

c) De protesto.

d) De actas de transferencia de bienes muebles registrables.

e) De actas y escrituras de procedimientos no contenciosos.

f) De instrumentos protocolares denominados de constitución de garantía mobiliaria y otras afectaciones sobre bienes muebles; y,

g) Otros que señale la ley.(*)

(*) Artículo modificado por la Quinta Disposición Complementaria Final del Decreto Legislativo N° 1332, publicado el 06 enero 2017, cuyo texto es el siguiente:

“ Artículo 37.- Registros Protocolares

Forman el protocolo notarial los siguientes registros:

a) De escrituras públicas.

b) De escrituras públicas unilaterales para la constitución de empresas, a través de los Centros de Desarrollo Empresarial autorizados por el Ministerio de la Producción.

c) De testamentos.

d) De protesto.

e) De actas de transferencia de bienes muebles registrables.

f) De actas y escrituras de procedimientos no contenciosos.

g) De instrumentos protocolares denominados de constitución de garantía mobiliaria y otras afectaciones sobre bienes muebles; y,

h) Otros que señale la ley” .

Artículo 38.- Forma de llevar los Registros
###########################################

El registro se compondrá de cincuenta fojas ordenadas correlativamente según su numeración.

Podrán ser llevados de dos maneras:

a) En veinticinco pliegos de papel emitido por el colegio de notarios, los mismos que se colocarán unos dentro de otros, de modo que las fojas del primer pliego sean la primera y la última; que las del segundo pliego sean la segunda y la penúltima y así sucesivamente; y,

b) En cincuenta hojas de papel emitido por el colegio de notarios, que se colocarán en el orden de su numeración seriada, para permitir el uso de sistemas de impresión computarizado. (*)

(*) Literal modificado por el Artículo 2 de la Ley N° 31338, publicada el 11 agosto 2021, cuyo texto es el siguiente:

"b) En cincuenta hojas de papel emitido por el colegio de notarios, que se colocarán en el orden de su numeración seriada, para permitir el uso de sistemas de impresión computarizado, de tecnología informática u otros de naturaleza similar, o para que el registro se efectúe a mano”.

Artículo 39.- Autorización de los Registros
###########################################

Cada registro será autorizado antes de su utilización, bajo responsabilidad del notario por el Colegio de Notarios al que pertenece, bajo el procedimiento y medidas de seguridad que éste fije.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 23

Artículo 40.- Foliación de los Registros
########################################

Las fojas de cada registro serán numeradas en forma correlativa, respetándose la serie de su emisión.

Artículo 41.- Formación de Tomos
################################

Se formará un tomo por cada diez registros, que deben encuadernarse y empastarse dentro del semestre siguiente a su utilización. Los tomos serán numerados en orden correlativo.

Artículo 42.- Conservación de los Registros
###########################################

El notario responderá del buen estado de conservación de los tomos.

Artículo 43.- Seguridad de los Registros
########################################

No podrán extraerse los registros y tomos de la oficina del notario, excepto por razones de fuerza mayor o cuando así se requiera para el cumplimiento de la función.

La exhibición, pericia, cotejo u otra diligencia por mandato judicial o del Ministerio Público, se realizará necesariamente en la oficina del notario.

Artículo 44.- Cierre de los Registros
#####################################

El treinta y uno de diciembre de cada año se cerrarán el registro, sentándose a continuación del último instrumento una constancia suscrita por el notario, la que remitirá, en copia, al colegio de notarios.

Si en el registro quedan fojas en blanco serán inutilizadas mediante dos líneas diagonales que se trazarán en cada página con la indicación que no corren.

CONCORDANCIAS :       D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 24

Artículo 45.- Extensión de Instrumentos Públicos
################################################

Los instrumentos públicos protocolares se extenderán observando riguroso orden cronológico, en los que consignará al momento de extenderse el número que les corresponda en orden sucesivo.

Artículo 46.- Forma de Extender un Instrumento Público
######################################################

Los instrumentos públicos protocolares se extenderán uno a continuación del otro.

Artículo 47.- Constancia de no conclusión de Instrumento Público
################################################################

Cuando no se concluya la extensión de un instrumento público protocolar o cuando luego de concluido y antes de su suscripción se advierta un error o la carencia de un requisito, el notario indicará en constancia que firmará, que el mismo no corre.

CONCORDANCIAS:       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 25

Artículo 48.- Intangibilidad de un Instrumento Público
######################################################

El instrumento público protocolar suscrito por los otorgantes y autorizado por un notario no podrá ser objeto de aclaración, adición o modificación en el mismo. Ésta se hará mediante otro instrumento público protocolar y deberá sentarse constancia en el primero, de haberse extendido otro instrumento que lo aclara, adiciona o modifica. En el caso que el instrumento que contiene la aclaración, adición o modificación se extienda ante distinto notario, éste comunicará esta circunstancia al primero, para los efectos del cumplimiento de lo dispuesto en este párrafo.

Cuando el notario advierta algún error en la escritura pública, en relación a su propia declaración, podrá rectificarla bajo su responsabilidad y a su costo, con un instrumento aclaratorio sin necesidad que intervengan los otorgantes, informándoseles del hecho al domicilio señalado en la escritura pública.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 26

Artículo 49.- Reposición del Instrumento Público
################################################

En caso de destrucción, deterioro, pérdida o sustracción parcial o total de un instrumento público protocolar, deberá informar este hecho al Colegio de Notarios y podrá solicitar la autorización para su reposición, sin perjuicio de la responsabilidad que corresponda.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 27

SECCIÓN PRIMERA

DEL REGISTRO DE ESCRITURAS PÚBLICAS

Artículo 50.- Registro de Escrituras Públicas
#############################################

En el registro de escrituras públicas se extenderán las escrituras, protocolizaciones y actas que la ley determina.

Artículo 51.- Definición
########################

Escritura pública es todo documento matriz incorporado al protocolo notarial, autorizado por el notario, que contiene uno o más actos jurídicos.

Artículo 52.- Partes de la Escritura Pública
############################################

La redacción de la escritura pública comprende tres partes:

a) Introducción.

b) Cuerpo; y,

c) Conclusión.

Artículo 53.- Introducción
##########################

Antes de la introducción de la escritura pública, el notario podrá indicar el nombre de los otorgantes y la naturaleza del acto jurídico.

Artículo 54.- Contenido de la Introducción
##########################################

La introducción expresará:

a) Lugar y fecha de extensión del instrumento.

b) Nombre del notario.

c) Nombre, nacionalidad, estado civil, domicilio y profesión u ocupación de los otorgantes; seguida de la indicación que proceden por su propio derecho.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 28, inc. a)

d) El documento nacional de identidad -D.N.I.- y los legalmente establecidos para la identificación de extranjeros.(*)

(*) Literal d) modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1236, publicado el 26 septiembre 2015, el mismo que entró en vigencia a los noventa (90) días hábiles de la publicación del Reglamento de la Ley en el Diario Oficial “El Peruano”, salvo disposición legal en contrario, cuyo texto es el siguiente:

" d) El documento nacional de identidad (DNI), los documentos de identidad o de viaje determinados para la identificación de extranjeros en el territorio nacional conforme a la normatividad sobre la materia, y la verificación de la respectiva categoría y calidad migratorias vigentes que lo autorice a contratar."(*)

(*) Literal d) modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1350, publicado el 07 enero 2017, el mismo que entró en vigencia el 1 de marzo de 2017, cuyo texto es el siguiente:

" d) El documento nacional de identidad - DNI, los documentos de identidad o de viaje determinados para la identificación de extranjeros en el territorio nacional conforme a la normatividad sobre la materia, y la verificación de la respectiva categoría y calidad migratorias vigentes que lo autorice a contratar."

e) La circunstancia de intervenir en el instrumento una persona en representación de otra, con indicación del documento que lo autoriza.(*)

(*) Literal modificado por la Cuarta Disposición Complementaria Modificatoria del Decreto Legislativo N° 1372, publicado el 02 agosto 2018, cuyo texto es el siguiente:

" e) La circunstancia de intervenir en el instrumento una persona en representación de otra, con indicación del documento que lo autoriza; así como, los datos de identificación del beneficiario final, conforme a la legislación de la materia."

f) La circunstancia de intervenir un intérprete en el caso de que alguno de los otorgantes ignore el idioma en el que se redacta el instrumento.

g) La indicación de intervenir una persona, llevada por el otorgante, en el caso de que éste sea analfabeto, no sepa o no pueda firmar, sea ciego o tenga otro defecto que haga dudosa su habilidad, sin perjuicio de que imprima su huella digital. A esta persona no le alcanza el impedimento de parentesco que señala esta Ley para el caso de intervención de testigos.(*)

(*) Literal modificado por el Artículo 7 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

" g) La indicación de intervenir de una persona, llevada por el otorgante, en el caso de que éste sea analfabeto, no sepa o no pueda firmar, sin perjuicio de que imprima su huella digital. A esta persona no le alcanza el impedimento de parentesco que señala esta Ley para el caso de intervención de testigos."

h) La fe del notario de la capacidad, libertad y conocimiento con que se obligan los otorgantes.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 28, inc. c)

i) La indicación de extenderse el instrumento con minuta o sin ella; y,(*)

(*) Literal modificado por el Artículo 7 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

" i) La indicación de intervenir de apoyos, a las personas que sean apoyos no les alcanza el impedimento de parentesco que señala esta Ley para el caso de intervención de testigos."

j) Cualquier dato requerido por ley, que soliciten los otorgantes o que sea necesario a criterio del notario.(*)

(*) Literal modificado por el Artículo 7 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

" j) La indicación de los ajustes razonables y salvaguardias requeridas por una persona con discapacidad."

" k) La indicación de extenderse el instrumento con minuta o sin ella.”(*)

(*) Literal incorporado por el Artículo 7 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

CONCORDANCIAS :      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 28

Artículo 55.- Identidad del Otorgante
#####################################

El notario dará fe de conocer a los otorgantes y/o intervinientes o de haberlos identificado.

Es obligación del notario acceder a la base de datos del Registro Nacional de Identificación y Estado Civil -RENIEC- en aquellos lugares donde se cuente con acceso a Internet y sea posible para la indicada entidad brindar el servicio de consultas en línea, para la verificación de la identidad de los intervinientes mediante la verificación de las imágenes, datos y/o la identificación por comparación biométrica de las huellas dactilares. Cuando el notario lo juzgue conveniente exigirá otros documentos y/o la intervención de testigos que garanticen una adecuada identificación.

CONCORDANCIAS :     D.S. Nº 010-2010-JUS (TUO del Reglamento)), Art. 29

El notario que diere fe de identidad de alguno de los otorgantes, inducido a error por la actuación maliciosa de los mismos o de otras personas, no incurrirá en responsabilidad.(*)

(*) Artículo modificado por la Sétima Disposición Complementaria Modificatoria del Decreto Legislativo Nº 1106, publicado el 19 abril 2012, cuyo texto es el siguiente:

“Artículo 55.- Identidad del Otorgante

El notario dará fe de conocer a los otorgantes y/o intervinientes o de haberlos identificado.

Es obligación del notario acceder a la base de datos del Registro Nacional de Identificación y Estado Civil - RENIEC- en aquellos lugares donde se cuente con acceso a Internet y sea posible para la indicada entidad brindar el servicio de consultas en línea, para la verificación de la identidad de los intervinientes mediante la verificación de las imágenes, datos y/o la identificación por comparación biométrica de las huellas dactilares. Cuando el notario lo juzgue conveniente exigirá otros documentos y/o la intervención de testigos que garanticen una adecuada identificación.

El notario que diere fe de identidad de alguno de los otorgantes, inducido a error por la actuación maliciosa de los mismos o de otras personas, no incurrirá en responsabilidad.(*)

(*) Artículo modificado por la Segunda Disposición Complementaria Final de la Ley N° 30313, publicada el 26 marzo 2015, cuyo texto es el siguiente:

“ Artículo 55.- Identidad del Otorgante

El notario dará fe de conocer a los otorgantes y/o intervinientes o de haberlos identificado.

Es obligación del notario verificar la identidad de los otorgantes o intervinientes, a través del acceso a la base de datos del Registro Nacional de Identificación y Estado Civil -RENIEC-, en aquellos lugares donde se cuente con acceso a Internet y sea posible para la indicada entidad brindar el servicio de consultas en línea, así como a la base de datos de la Superintendencia Nacional de Migraciones, respecto de la información sobre los extranjeros residentes o no en el país, pudiendo acceder al registro de carnés de extranjería, pasaportes y control migratorio de ingreso de extranjeros, para la verificación de la identidad de los intervinientes mediante la verificación de las imágenes, datos y/o la identificación por comparación biométrica de las huellas dactilares. Cuando el notario lo juzgue conveniente exigirá otros documentos y/o la intervención de testigos que garanticen una adecuada identificación.

Para estos efectos, el ejercicio personal de la función no excluye la colaboración de dependientes del despacho notarial, sin que ello implique la delegación de la función para realizar los actos complementarios o conexos que coadyuven al desarrollo de su labor, bajo la responsabilidad exclusiva del notario.

El notario que cumpliendo los procedimientos establecidos en el presente artículo diere fe de identidad de alguno de los otorgantes, inducido a error por la actuación maliciosa de los mismos o de otras personas, no incurre en responsabilidad."

Asimismo, el notario público deberá dejar expresa constancia en la escritura pública de haber efectuado las mínimas acciones de control y debida diligencia en materia de prevención del lavado de activos, especialmente vinculado a la minería ilegal u otras formas de crimen organizado, respecto a todas las partes intervinientes en la transacción, específicamente con relación al origen de los fondos, bienes u otros activos involucrados en dicha transacción, así como con los medios de pago utilizados.” (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 55.- Identidad del Otorgante

El notario dará fe de conocer a los otorgantes y/o intervinientes o de haberlos identificado, conforme a lo siguiente:

a) Cuando en el distrito donde se ubica el oficio notarial tenga acceso a internet, el notario exigirá el documento nacional de identidad y deberá verificar la identidad de los otorgantes o intervinientes utilizando la comparación biométrica de las huellas dactilares, a través del servicio que brinda el Registro Nacional de Identidad y Estado Civil - RENIEC.

b) Cuando no se pueda dar cumplimiento a lo señalado en el literal a) del presente artículo respecto a la comparación biométrica de las huellas dactilares por causa no imputable al notario, éste exigirá el documento nacional de identidad y la consulta en línea para la verificación de las imágenes y datos del Registro Nacional de Identidad y Estado Civil - RENIEC con la colaboración del Colegio de Notarios respectivo, si fuera necesaria. El notario podrá recurrir adicionalmente a otros documentos y/o la intervención de testigos que garanticen una adecuada identificación.

c) Tratándose de extranjeros residentes o no en el país, el notario exigirá el documento oficial de identidad, y además, accederá a la información de la base de datos del registro de carnés de extranjería, pasaportes y control migratorio de ingreso de extranjeros; en tanto sea implementado por la Superintendencia Nacional de Migraciones, conforme a la décima disposición complementaria, transitoria y final de la presente ley. Asimismo, de juzgarlo conveniente podrá requerir otros documentos y/o la intervención de testigos que garanticen una adecuada identificación.

d) Excepcionalmente y por razón justificada, el notario podrá dar fe de conocimiento o de identidad sin necesidad de seguir los procedimientos señalados en los literales a) y b) del presente artículo. En este caso, el notario incurre en las responsabilidades de ley cuando exista suplantación de la identidad.

El notario que cumpliendo los procedimientos establecidos en los literales a), b) y c) del presente artículo diere fe de identidad de alguno de los otorgantes, inducido a error por la actuación maliciosa de los mismos o de otras personas, no incurre en responsabilidad, sin perjuicio de que se declare judicialmente la nulidad del instrumento.

En el instrumento público protocolar suscrito por el otorgante y/o interviniente, el notario deberá dejar expresa constancia de las verificaciones a las que se refiere el presente artículo o la justificación de no haber seguido el procedimiento.” (*)

(*) Mediante Oficio N° 368-2019-JUS-OGAJ, enviado por la Oficina General de Asesoría Jurídica del Ministerio de Justicia y Derechos Humanos se indica que mediante la Ley N° 30908 se amplían los alcances del presente artículo, sobre la identidad del otorgante en la sección del registro de escrituras públicas.  (*)

CONCORDANCIAS:      D.S. N° 017-2012-JUS (Decreto Supremo que establece la obligatoriedad del uso del sistema de verificación biométrica de huellas dactilares                 en todos los oficios notariales del país)
R.N° 265-2016-SUNARP-SN (Aprueban formulario de solicitud de oposición de título en trámite)

Artículo 56.- Impedimentos para ser testigo
###########################################
Para intervenir como testigo se requiere tener la capacidad de ejercicio de sus derechos civiles y no estar incurso en los siguientes impedimentos:

a) Ser sordo, ciego y mudo.(*)

(*) Literal derogado por el Literal c) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

b) Ser analfabeto.

c) Ser cónyuge, ascendiente, descendiente o hermano del compareciente.

d) Ser cónyuge o pariente del notario dentro del cuarto grado de consanguinidad o segundo de afinidad; y,

e) Los que a juicio del notario no se identifiquen plenamente.

f) Ser dependiente del Notariado.

Al testigo, cuyo impedimento no fuere notorio al tiempo de su intervención, se le tendrá como hábil si la opinión común así lo hubiera considerado.

Artículo 57.- Contenido del Cuerpo de la Escritura
##################################################

El cuerpo de la escritura contendrá:

a) La declaración de voluntad de los otorgantes, contenida en minuta autorizada por letrado, la que se insertará literalmente.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 30

b) Los comprobantes que acrediten la representación, cuando sea necesaria su inserción.

c) Los documentos que los otorgantes soliciten su inserción.

d) Los documentos que por disposición legal sean exigibles.

e) Otros documentos que el notario considere convenientes.

Artículo 58.- Inexigencia de la Minuta
######################################

No será exigible la minuta en los actos siguientes:

a) Otorgamiento, aceptación, sustitución, revocación y renuncia del poder.

b) Renuncia de nacionalidad.

c) Nombramiento de tutor y curador en los casos que puede hacerse por escritura pública.

d) Reconocimiento de hijos.

e) Autorización para el matrimonio de menores de edad otorgada por quienes ejercen la patria potestad.

f) Aceptación expresa o renuncia de herencia.

g) Declaración jurada de bienes y rentas.

h) Donación de órganos y tejidos.

i) Constitución de micro y pequeñas empresas.

CONCORDANCIAS:      D.S. Nº 007-2008-TR, Art. 9 (Simplificación de trámites y régimen de ventanilla única)

j) Hipoteca unilateral; y,

k) Otros que la ley señale.(*)

(*) Literal k) modificado por la Tercera Disposición Complementaria Final de la Ley N° 30933, publicada el 24 abril 2019, cuyo texto es el siguiente:

" k) Arrendamiento de inmuebles sujetos a la Ley que regula el procedimiento especial de desalojo con intervención notarial."

" l) Otros que la ley señale” .(*)

(*) Literal l) incorporado por la Tercera Disposición Complementaria Final de la Ley N° 30933, publicada el 24 abril 2019.

Artículo 59.- Conclusión de la Escritura Pública
################################################

La conclusión de la escritura expresará:

a) La fe de haberse leído el instrumento, por el notario o los otorgantes, a su elección.

b) La ratificación, modificación o indicaciones que los otorgantes hicieren, las que también serán leídas.

c) La fe de entrega de bienes que se estipulen en el acto jurídico.

d) La transcripción literal de normas legales, cuando en el cuerpo de la escritura se cite sin indicación de su contenido y están referidos a actos de disposición u otorgamiento de facultades.

e) La transcripción de cualquier documento o declaración que sea necesario y que pudiera haberse omitido en el cuerpo de la escritura.

f) La intervención de personas que sustituyen a otras, por mandato, suplencia o exigencia de la ley, anotaciones que podrán ser marginales.

g) Las omisiones que a criterio del notario deban subsanarse para obtener la inscripción de los actos jurídicos objeto del instrumento y que los otorgantes no hayan advertido.

h) La corrección de algún error u omisión que el notario o los otorgantes adviertan en el instrumento.

i) La constancia del número de serie de la foja donde se inicia y de la foja donde concluye el instrumento; y,

j) La impresión dactilar y suscripción de todos los otorgantes así como la suscripción del notario, con indicación de la fecha en que firma cada uno de los otorgantes así como cuando concluye el proceso de firmas del instrumento.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 59 .- Conclusión de la Escritura Pública

La conclusión de la escritura expresará:

a) La fe de haberse leído el instrumento, por el notario o los otorgantes, a su elección.

b) La ratificación, modificación o indicaciones que los otorgantes hicieren, las que también serán leídas.

c) La fe de entrega de bienes que se estipulen en el acto jurídico.

d) La transcripción literal de normas legales, cuando en el cuerpo de la escritura se cite sin indicación de su contenido y están referidos a actos de disposición u otorgamiento de facultades.

e) La transcripción de cualquier documento o declaración que sea necesario y que pudiera haberse omitido en el cuerpo de la escritura.

f) La intervención de personas que sustituyen a otras, por mandato, suplencia o exigencia de la ley, anotaciones que podrán ser marginales.

g) Las omisiones que a criterio del notario deban subsanarse para obtener la inscripción de los actos jurídicos objeto del instrumento y que los otorgantes no hayan advertido.

h) La corrección de algún error u omisión que el notario o los otorgantes adviertan en el instrumento.

i) La constancia del número de serie de la foja donde se inicia y de la foja donde concluye el instrumento; y,

j) La impresión dactilar y suscripción de todos los otorgantes así como la suscripción del notario, con indicación de la fecha en que firma cada uno de los otorgantes así como cuando concluye el proceso de firmas del instrumento.

k) La constancia de haber efectuado las mínimas acciones de control y debida diligencia en materia de prevención del lavado de activos, especialmente vinculado a la minería ilegal u otras formas de crimen organizado, respecto a todas las partes intervinientes en la transacción, específicamente con relación al origen de los fondos, bienes u otros activos involucrados en dicha transacción, así como con los medios de pago utilizados” .

Artículo 60.- Minutario
#######################

En las minutas se anotará la foja del registro y la fecha en que se extendió el instrumento.

Se formará un tomo de minutas cuando su cantidad lo requiera, ordenándolas según el número que les corresponda.

Los tomos se numerarán correlativamente.

Artículo 61.- Autorización de Instrumento Público Posterior al Cese
###################################################################

Si el notario ha cesado en el cargo sin haber autorizado una escritura pública o acta notarial protocolar, cuando aquella se encuentre suscrita por todos los intervinientes, puede cualquier interesado pedir por escrito al colegio de notarios encargado del archivo, que designe a un notario, para que autorice el instrumento público, con indicación de la fecha en que se verifica este acto y citando previamente a las partes.

Artículo 62.- Designación de Notario que Autorizará Instrumento Público Posterior al Cese.
##########################################################################################

En el caso de que el notario ha cesado en el cargo y la escritura o acta notarial protocolar no haya sido suscrita por ninguno o alguno de los otorgantes, podrán éstos hacerlo solicitándolo por escrito al colegio de notarios encargado del archivo, para que designe un notario, quien dará fe de este hecho y autorizará la escritura con indicación de la fecha en que se verifica este acto.

Artículo 63.- Transferencia de los Archivos
###########################################

Transcurridos dos (2) años de ocurrido el cese del notario, los archivos notariales serán transferidos al Archivo General de la Nación o a los archivos departamentales, de conformidad con el artículo 5 del Decreto Ley Nº 19414 y el artículo 9 de su Reglamento.

Artículo 64.- Protocolización
#############################

Por la protocolización se incorporan al registro de escrituras públicas los documentos que la ley, resolución judicial o administrativa ordenen.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art.33

Artículo 65.- Contenido del Acta de Protocolización
###################################################

El acta de protocolización contendrá:

a) Lugar, fecha y nombre del notario.

b) Materia del documento.

c) Los nombres de los intervinientes.

d) El número de fojas de que conste; y,

e) Nombre del juez que ordena la protocolización y del secretario cursor y mención de la resolución que ordena la protocolización con la indicación de estar consentida o ejecutoriada o denominación de la entidad que solicita la protocolización.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 65.- Contenido del Acta de Protocolización

El acta de protocolización contendrá:

a) Lugar, fecha y nombre del notario.

b) Materia del documento.

c) Los nombres de los intervinientes.

d) El número de fojas de que conste.

e) Nombre del juez que ordena la protocolización y del secretario cursor y mención de la resolución que ordena la protocolización con la indicación de estar consentida o ejecutoriada o denominación de la entidad que solicita la protocolización.

f) Tratándose de la protocolización de laudos arbitrales deberá requerirse la comparecencia del árbitro o uno de ellos designados por el Tribunal Arbitral para su identificación.”

Artículo 66.- Adjuntos a la Protocolización
###########################################

El notario agregará los documentos materia de la protocolización al final del tomo donde corre sentada el acta de protocolización.

Los documentos protocolizados no podrán separarse del registro de escrituras públicas por ningún motivo.

SECCIÓN SEGUNDA:

DEL REGISTRO DE TESTAMENTOS

Artículo 67.- Definición
########################

En este registro se otorgará el testamento en escritura pública y cerrado que el Código Civil señala.

Será llevado en forma directa por el notario, para garantizar la reserva que la presente ley establece para estos actos jurídicos.

Artículo 68.- Formalidad del Registro de Testamento
###################################################

El notario observará en el otorgamiento del testamento en escritura pública y el cerrado las formalidades prescritas por el Código Civil.

Artículo 69.- Observaciones al Registro de Testamento
#####################################################

Son también de observancia para el registro de testamentos las normas que preceden en este Título, en cuanto sean pertinentes.

Artículo 70.- Remisión de relación de testamentos
#################################################

El notario remitirá al colegio de notarios, dentro de los primeros ochos(*)NOTA SPIJ días de cada mes, una relación de los testamentos en escritura pública y cerrados extendidos en el mes anterior.

Para tal efecto, llevará un libro de cargos, que será exhibido en toda visita de inspección.

Artículo 71.- Conocimiento del Testamento
#########################################

Se prohíbe al notario y al colegio de notarios informar o manifestar el contenido o existencia de los testamentos mientras viva el testador.

El informe o manifestación deberá hacerse por el notario con la sola presentación del certificado de defunción del testador.

Artículo 72.- Traslados de testamentos
######################################

El testimonio o boleta del testamento, en vida del testador, sólo será expedido a solicitud de éste.

Artículo 73.- Inscripción del Testamento
########################################

El notario solicitará la inscripción del testamento en escritura pública al registro de testamentos que corresponda, mediante parte que contendrá la fecha de su otorgamiento, fojas donde corre extendido en el registro, nombre del notario, del testador y de los testigos, con la constancia de su suscripción.

En caso de revocatoria, indicará en el parte esta circunstancia.

CONCORDANCIAS:      R. Nº 156-2012-SUNARP-SN, Art. 10

Artículo 74.- El Testamento
###########################

Tratándose del testamento cerrado el notario transcribirá al registro de testamentos que corresponda, copia literal del acta transcrita en su registro, con indicación de la foja donde corre.

En caso de revocatoria del testamento cerrado transcribirá al registro de testamentos que corresponda, el acta en la que consta la restitución al testador del testamento cerrado, con indicación de la foja donde corre.

CONCORDANCIAS:      R. Nº 156-2012-SUNARP-SN, Art. 10

SECCIÓN TERCERA:

DEL REGISTRO DE PROTESTOS

Artículo 75.- Registro de Protestos
###################################

En este registro se anotarán los protestos de títulos valores, asignando una numeración correlativa a cada título, según el orden de presentación por parte de los interesados para los fines de su protesto, observando las formalidades señaladas en la ley de la materia.

Igualmente, en este mismo registro se anotarán los pagos parciales, negación de firmas en los títulos valores protestados u otras manifestaciones que deseen dejar constancia las personas a quienes se dirija la notificación del protesto, en el curso del día de dicha notificación y hasta el día hábil siguiente.

CONCORDANCIAS :       D.S. Nº 010-2010-JUS (TUO del Reglamento), Art.18

Artículo 76.- Formalidad del Registro
#####################################

El registro puede constar en libros, o en medios electrónicos o similares que aseguren la oportunidad de sus anotaciones, observando las normas precedentes al presente Título en cuanto resulten pertinentes.

Artículo 77.- Registros separados
#################################

Se podrán llevar registros separados para títulos valores sujetos a protesto por falta de aceptación, por falta de pago y otras obligaciones; y por tipo de título valor, expidiendo certificaciones a favor de quienes lo soliciten.

SECCIÓN CUARTA:

DEL REGISTRO DE ACTAS DE TRANSFERENCIA DE BIENES MUEBLES REGISTRABLES

Artículo 78.- Registro de Actas de Transferencia de Bienes Muebles Registrables
###############################################################################

En este registro se extenderán las actas de transferencia de bienes muebles registrables, que podrán ser:

a) De vehículos; y,

b) De otros bienes muebles identificables y/o incorporados a un registro jurídico, que la ley determine.

Artículo 79.- Observancias del registro de Actas de Transferencia de Bienes Muebles
###################################################################################

Son también de observancia para el registro de actas de transferencia de bienes muebles registrables, las normas que preceden en este Título, en cuanto sean pertinentes.

Artículo 80.- Formalidad del Acta de Transferencia
##################################################

Las actas podrán constar en registros especializados en razón de los bienes muebles materia de la transferencia y en formularios impresos para tal fin.

SECCIÓN QUINTA:

DEL ARCHIVO NOTARIAL Y DE LOS TRASLADOS

Artículo 81.- El Archivo Notarial
#################################

El archivo notarial se integra por:

a) Los registros físicos, en soporte de papel o medio magnético, que lleva el notario conforme a ley.

b) Los tomos de minutas extendidas en el registro.

c) Los documentos protocolizados conforme a ley; y,

d) Los índices que señala esta ley.

Artículo 82.- Responsabilidad en la Expedición de Instrumentos Públicos
#######################################################################

El notario expedirá, bajo responsabilidad, testimonio, boleta y partes, a quien lo solicite, de los instrumentos públicos notariales que hubiera autorizado en el ejercicio de su función.

Asimismo, expedirá copias certificadas de las minutas que se encuentren en su archivo notarial.

Los traslados notariales a que se refiere este artículo podrán efectuarse en formato digital o medios físicos que contengan la información del documento matriz de manera encriptada y segura y que hagan factible su verificación a través de los mecanismos tecnológicos disponibles.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 34

Asimismo el notario podrá emitir un traslado notarial remitido electrónicamente por otro notario e impreso en su oficio notarial, siempre que los mensajes electrónicos se trasladen por un medio seguro y al amparo a la legislación de firmas y certificados digitales.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 34

Las copias electrónicas se entenderán siempre expedidas por el Notario autorizante del documento matriz y no perderán su carácter, valor y efectos por el solo hecho de ser trasladados a formato papel por el notario al que se le hubiere enviado el documento; el mismo que deberá firmarlo y rubricarlo haciendo constar su carácter y procedencia.

Artículo 83.- El Testimonio
###########################

El testimonio contiene la transcripción íntegra del instrumento público notarial con la fe que da el notario de su identidad con la matriz, la indicación de su fecha y foja donde corre, la constancia de encontrarse suscrito por los otorgantes y autorizado por él, rubricado en cada una de sus fojas y expedido con su sello y firma, con la mención de la fecha en que lo expide.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 35

Artículo 84.- La Boleta
#######################

La boleta expresará un resumen del contenido del instrumento público notarial o transcripción de las cláusulas o términos que el interesado solicite y que expide el notario, con designación del nombre de los otorgantes, naturaleza del acto jurídico, fecha y foja donde corre y la constancia de encontrarse suscrito por los otorgantes y autorizado por él, rubricada en cada una de sus fojas y expedida con su sello y firma, con mención de la fecha en que la expide.

El notario, cuando lo considere necesario, agregará cualquier referencia que dé sentido o complete la transcripción parcial solicitada.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 35

Artículo 85.- El Parte
######################

El parte contiene la transcripción íntegra del instrumento público notarial con la fe que da el notario de su identidad con la matriz, la indicación de su fecha y con la constancia de encontrarse suscrito por los otorgantes y autorizado por él, rubricado en cada una de sus fojas y expedido con su sello y firma, con la mención de la fecha en que lo expide.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 85.- El Parte

El parte contiene la transcripción íntegra del instrumento público notarial con la fe que da el notario de su identidad con la matriz, la indicación de su fecha y con la constancia de encontrarse suscrito por los otorgantes y autorizado por él, rubricado en cada una de sus fojas y expedido con su sello y firma, con la mención de la fecha en que lo expide.

El parte debe constar en papel notarial de seguridad que incorpore características especiales que eviten la falsificación o alteración de su contenido."

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 35

D.S. Nº 070-2011-PCM, Art. 5 (Presentación y tramitación de partes notariales electrónicos firmados digitalmente)

Artículo 86.- Expedición de Traslados Notariales
################################################

El testimonio, boleta y parte podrá expedirse, a elección del notario, a manuscrito, mecanografiado, en copia fotostática y por cualquier medio idóneo de reproducción.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 86.- Expedición de Traslados Notariales

El testimonio, boleta y parte podrá expedirse, a elección del notario, a manuscrito, mecanografiado, en copia fotostática y por cualquier medio idóneo de reproducción.

Los testimonios, las boletas y los partes expedidos conforme a lo previsto en los artículos 83, 84 y 85 de la presente Ley en el caso de remitirse en formato digital deberán, además, cumplir con las condiciones y requisitos de la Ley de la materia” .

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 35

D.S. Nº 070-2011-PCM, Art. 5 (Presentación y tramitación de partes notariales electrónicos firmados digitalmente)

Artículo 87.- Obligación de Expedir Traslados
#############################################

Si es solicitado el traslado de un instrumento público notarial y el notario niega su existencia en el registro, el interesado podrá recurrir al Colegio de Notarios respectivo, para que éste ordene el examen del índice y registro y comprobada su existencia, ordene la expedición del traslado correspondiente.

Artículo 88.- Excepción
#######################

El notario podrá expedir traslados de instrumentos públicos notariales no inscritos o con la constancia de estar en trámite su inscripción.

Artículo 89.- Designación de Notario para la Autorización de Traslados
######################################################################

Cuando el colegio de notarios esté encargado del archivo designará a un notario autorice los traslados a que se refieren los artículos que preceden.

Artículo 90.- Expedición de Constancia a Solicitud de Parte
###########################################################

A solicitud de parte el notario expedirá constancia que determinado instrumento público notarial no ha sido suscrito por alguno o todos los otorgantes, para los fines legales consiguientes.

Artículo 91.- Índices
#####################

El notario llevará índices cronológico y alfabético de instrumentos públicos protocolares, a excepción del registro de protestos que solo llevará el índice cronológico.

El índice consignará los datos necesarios para individualizar cada instrumento.

Estos índices podrán llevarse en tomos o en hojas sueltas, a elección del notario, en el caso de llevarse en hojas sueltas deberá encuadernarse y empastarse dentro del semestre siguiente a su formación.

Asimismo, podrá llevar estos registros a través de archivos electrónicos, siempre y cuando la información de los mismos sea suministrada empleando la tecnología de firmas y certificados digitales de conformidad con la legislación de la materia.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 37

Artículo 92.- Responsabilidad en la Conservación de Archivos
############################################################

El notario responderá del buen estado de conservación de los archivos e índices.

Artículo 93.- Obligación de Manifestar Documentos
#################################################

El notario está obligado a manifestar los documentos de su archivo a cuantos tengan interés de instruirse de su contenido.

Esta manifestación se realizará bajo las condiciones de seguridad que el notario establezca.
