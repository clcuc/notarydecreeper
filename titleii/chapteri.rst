##################################
CAPÍTULO I DISPOSICIONES GENERALES
##################################

Artículo 23.- Definición
########################

Son instrumentos públicos notariales los que el notario, por mandato de la ley o a solicitud de parte, extienda o autorice en ejercicio de su función, dentro de los límites de su competencia y con las formalidades de ley.

Artículo 24.- Fe Pública
########################

Los instrumentos públicos notariales otorgados con arreglo a lo dispuesto en la ley, producen fe respecto a la realización del acto jurídico y de los hechos y circunstancias que el notario presencie.

Asimismo, producen fe aquellos que autoriza el notario utilizando la tecnología de firmas y certificados digitales de acuerdo a la ley de la materia.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 17

Artículo 25.- Instrumentos Públicos Protocolares
################################################

Son instrumentos públicos protocolares las escrituras públicas, instrumentos y demás actas que el notario incorpora al protocolo notarial; que debe conservar y expedir los traslados que la ley determina.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art.18

Artículo 26.- Instrumentos Públicos Extraprotocolares
#####################################################

Son instrumentos públicos extraprotocolares las actas y demás certificaciones notariales que se refieren a actos, hechos o circunstancias que presencie o le conste al notario por razón de su función.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 19

Artículo 27.- Efectos
#####################

El notario cumplirá con advertir a los interesados sobre los efectos legales de los instrumentos públicos notariales que autoriza. En el caso de los instrumentos protocolares dejará constancia de este hecho.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 5

Artículo 28.- Idioma
####################

Los instrumentos públicos notariales se extenderán en castellano o en el idioma que la ley permita.

Artículo 29.- Limitaciones en la aplicación
###########################################

Quedan exceptuadas de lo dispuesto en el artículo anterior las palabras, aforismos y frases de conocida aceptación jurídica.

Artículo 30.- Aplicación de Otros Idiomas
#########################################

Cuando alguno de los interesados no conozca el idioma usado en la extensión del instrumento, el notario exigirá la intervención de intérprete, nombrado por la parte que ignora el idioma, el que hará la traducción simultánea, declarando bajo su responsabilidad en el instrumento público la conformidad de la traducción.

El notario a solicitud expresa y escrita del otorgante, insertará el texto en el idioma del interesado o adherirlo, en copia legalizada notarialmente, al instrumento original, haciendo mención de este hecho.(*)

(*) Artículo modificado por el Artículo 7 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“ Artículo 30.- Aplicación de otros idiomas

Cuando alguno de los interesados no conozca el idioma usado en la extensión del instrumento, el notario exige la intervención de intérprete, nombrado por la parte que ignora el idioma, el que hace la traducción simultánea, declarando bajo su responsabilidad en el instrumento público la conformidad de la traducción.

De igual modo, se debe asegurar la intervención de un intérprete para sordos o un guía intérprete en caso de las personas sordociegas, de ser necesario.

El notario a solicitud expresa y escrita del otorgante, inserta el texto en el idioma del interesado o adherirlo, en copia legalizada notarialmente, al instrumento original, haciendo mención de este hecho.”

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 20

Artículo 31.- Forma de Extender un Instrumento Público
######################################################

Los instrumentos públicos notariales deberán extenderse con caracteres legibles, en forma manuscrita, o usando cualquier medio de impresión que asegure su permanencia.

Artículo 32.- Espacios en Blanco
################################

Los instrumentos públicos notariales no tendrán espacios en blanco. Éstos deberán ser llenados con una línea doble que no permita agregado alguno.

No existe obligación de llenar espacios en blanco, únicamente cuando se trate de documentos insertos o anexos, que formen parte del instrumento público notarial y que hayan sido impresos mediante fotocopiado, escaneado u otro medio similar bajo responsabilidad del notario.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 21

Artículo 33.- Equivocaciones en un Instrumento Público
######################################################

Se prohíbe en los instrumentos públicos notariales, raspar o borrar las equivocaciones por cualquier procedimiento. Las palabras, letras, números o frases equivocadas deberán ser testados y se cubrirán con una línea de modo que queden legibles y se repetirán antes de la suscripción, indicándose que no tienen valor.

Los interlineados deberán ser transcritos literalmente antes de la suscripción, indicándose su validez; caso contrario se tendrán por no puestos.

Artículo 34.- Redacción de un Instrumento Público
#################################################

En la redacción de instrumentos públicos notariales se podrán utilizar guarismos, símbolos y fórmulas técnicas.

No se emplean abreviaturas ni iniciales, excepto cuando figuren en los documentos que se inserten.

Artículo 35.- Fechas del instrumento público
############################################

La fecha del instrumento y la de su suscripción, cuando fuere el caso, constarán necesariamente en letras.

Deberá constar necesariamente en letras y en número, el precio, capital, área total, cantidades que expresen los títulos valores; así como porcentajes, participaciones y demás datos que resulten esenciales para la seguridad del instrumento a criterio del notario.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 22
