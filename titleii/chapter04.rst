##########################
CAPÍTULO IV DE LOS PODERES
##########################

Artículo 117.- Clases de Poderes
################################

Los poderes ante notario podrán revestir las siguientes modalidades:

a) Poder en escritura pública.

b) Poder fuera de registro; y,

c) Poder por carta con firma legalizada.

El notario llevará un índice cronológico que incluya todos los poderes otorgados fuera de registro.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art.54

Artículo 118.- Poder por Escritura Pública
##########################################

El poder por escritura pública se rige por las disposiciones establecidas en la Sección Primera del Título II de la presente ley.

La modificatoria o revocatoria de poder otorgado ante otro notario deberá ser informada por el notario que extienda la escritura pública al notario donde se extendió la escritura de poder.

Artículo 119.- Poder Fuera de Registro
######################################

El poder fuera de registro se rige por las disposiciones a que se refiere el artículo anterior, sin requerir para su validez de su incorporación al protocolo notarial.

Artículo 120.- Poder por Carta
##############################

El poder por carta con firma legalizada, se otorga en documento privado, conforme las disposiciones sobre la materia.

Respecto a asuntos inherentes al cobro de beneficios de derechos laborales, seguridad social en salud y pensiones, el poder por carta con firma legalizada tiene una validez de tres meses para cantidades menores a media Unidad Impositiva Tributaria.

Artículo 121.- Transcripción de normas legales
##############################################

Cuando en los poderes en escritura pública y fuera de registro, se cite normas legales, sin indicación de su contenido y estén referidas a actos de disposición u otorgamiento de facultades, el notario transcribirá literalmente las mismas.

Artículo 122.- Modalidades de poder por Cuantía
###############################################

El uso de cada una de estas modalidades de poder estará determinado en razón de la cuantía del encargo.

En caso de no ser éste susceptible de valuación, regirán las normas sobre el derecho común.
