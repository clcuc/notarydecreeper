.. Notary law documentation master file, created by
   sphinx-quickstart on Mon Dec 06 03:26:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Notary Decree
==================

SPIJ: CONSTITUCION POLITICA, LEYES ORGANICAS Y CODIGOS\NORMAS LEGALES\2008\JUNIO\Jueves, 26 de junio de 2008\DECRETOS LEGISLATIVOS

Sector: DECRETOS LEGISLATIVOS

Fecha de Publicación: 26 de junio de 2008

Decreto Legislativo del Notariado  
DECRETO LEGISLATIVO Nº 1049

CONCORDANCIAS:      D.S. N° 010-2010-JUS (TUO del Reglamento del Decreto Legislativo Nº 1049, Decreto Legislativo del Notariado)

D.S. Nº 003-2009-JUS (REGLAMENTO)

D.S. N° 015-2008-JUS (Aprueban Reglamento del Concurso Público de Méritos para el Ingreso a la Función Notarial)  
OTRAS CONCORDANCIAS

Enlace Web: EXPOSICIÓN DE MOTIVOS - PDF.

EL PRESIDENTE DE LA REPÚBLICA

POR CUANTO:

El Congreso de la República, de conformidad con el Artículo 104 de la Constitución Política del Perú, mediante la Ley Nº 29157 ha delegado en el Poder Ejecutivo la facultad de legislar sobre materias específicas con la finalidad de facilitar la implementación del Acuerdo de Promoción Comercial Perú-Estados Unidos y su protocolo de enmienda así como el apoyo a la competitividad económica para su aprovechamiento, encontrándose dentro de las materias comprendidas en dicha delegación la facilitación del comercio; la promoción de la inversión privada; el impulso a la innovación tecnológica, la mejora de la calidad y el desarrollo de capacidades; y la promoción de las micro, pequeñas y medianas empresas;

Que, el desarrollo del comercio y la promoción tanto de la inversión privada nacional como extranjera así como la formalización de micro, pequeñas y medianas empresas deben contar con una seguridad y publicidad jurídicas que permitan garantizar la cognoscibilidad general de derechos inscribibles o de actos con relevancia registral, lo que implica la modernización de instituciones del Estado, así como de los de operadores adscritos o que actúan por delegación de éste, que, dentro del ordenamiento jurídico, garantizan la seguridad de los actos y transacciones inscribibles, siendo necesario por ello dictar la ley correspondiente que conlleve una mejora en el ejercicio y supervisión de la función notarial, por ser el notario el profesional en Derecho autorizado para dar fe pública por delegación del Estado, a los actos y contratos que ante él se celebren; adecuándolo a los últimos cambios tecnológicos para facilitar las transacciones y el intercambio comercial mediante canales seguros;

Con el voto aprobatorio del Consejo de Ministros; y,

Con cargo a dar cuenta al Congreso de la República;

Ha dado el Decreto Legislativo siguiente:

DECRETO LEGISLATIVO DEL NOTARIADO

.. toctree::
   :maxdepth: 2
   :caption: T1 El Notariado y la Funcion Notarial

   Disposiciones Generales              <titlei/chapteri>
   Ingreso a la Funcion Notarial        <titlei/chapterii>
   Deberes del Notario                  <titlei/chapteriii>
   Prohibiciones del Notario            <titlei/chapteriv>
   Derechos del Notario                 <titlei/chapterv>
   Cese del Notario                     <titlei/chaptervi>

.. toctree::
   :maxdepth: 2
   :caption: T2 Los instrumentos publicos notariales

   Disposiciones Generales              <titleii/chapteri>
   Instrumentos Publicos Protocolares   <titleii/chapterii>
   Instr Publ Extraprotocolares         <titleii/chapter03>
   Poderes                              <titleii/chapter04>
   Nulidad de los Instr Publ Notariales <titleii/chapter05>

.. toctree::
   :maxdepth: 2
   :caption: T3 Organizacion del Notariado

   Distrito Notarial                    <title03/chapter01>
   Colegios de Notarios                 <title03/chapter02>
   Junta de Decanos de los Colegios     <title03/chapter03>
   Consejo del Notariado                <title03/chapter04>

.. toctree::
   :maxdepth: 2
   :caption: T4 Vigilancia del Notariado

   Responsabili en Ejercicio de Funcion <title04/chapter01>
   Regimen Disciplinario                <title04/chapter02>
   Infracciones Administ Disciplinarias <title04/chapter03>
   Sanciones, Proced, y Medida Cautelar <title04/chapter04>
   Prescripcion de Accion Disciplinaria <title04/chapter05>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
