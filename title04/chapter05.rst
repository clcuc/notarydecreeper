########################################################
CAPÍTULO V DE LA PRESCRIPCIÓN DE LA ACCIÓN DISCIPLINARIA
########################################################

Artículo 154.- Plazo de prescripción
####################################

La acción disciplinaria prescribe a los cinco (5) años, contados desde el día en que se cometió la presunta infracción administrativa disciplinaria. El inicio del proceso disciplinario y/o la existencia de un proceso penal interrumpen el término de la prescripción.

Artículo 155.- Responsabilidad del Notario Posterior al Cese
############################################################

El proceso disciplinario y la sanción procederán aún cuando el notario haya cesado en el cargo.

Artículo 156.- Registro de Sanciones
####################################

Toda sanción se anotará, una vez firme, en el legajo de antecedentes del notario.

DISPOSICIONES COMPLEMENTARIAS, TRANSITORIAS Y FINALES
#####################################################

Primera.- En tanto no se elijan tribunales de honor en los colegios de notarios, las juntas directivas tendrán competencia para conocer y resolver en primera instancia todas las denuncias y procedimientos disciplinarios, con las atribuciones y responsabilidades correspondientes, hasta la culminación de los mismos.

Segunda.- La Junta de Decanos de los Colegios de Notarios del Perú coordinará con los colegios de notarios de la República la adecuación del Estatuto Único a lo que establece la presente norma.

Tercera.- Quedan sin efecto los concursos públicos convocados a la fecha. En un plazo máximo de noventa (90) días todos los colegios deben convocar a concurso público la totalidad de sus plazas vacantes existentes a la fecha, bajo responsabilidad; reconociendo y/o devolviendo los derechos abonados en los concursos dejados sin efecto, a elección de los interesados.(*) RECTIFICADO POR FE DE ERRATAS

CONCORDANCIAS:      D.S. N° 015-2008-JUS, 1ra. Disp. Trans. y Final (Plazo en días hábiles)

Cuarta.- El Consejo del Notariado aprobará las disposiciones que sean necesarias para la implementación gradual de la obligación prevista en el inciso i) del artículo 16 de la presente norma.

Quinta.- En el caso de inscripciones sustentadas en partes o escrituras públicas presumiblemente falsificadas, el notario ante quien supuestamente se habría otorgado dicho instrumento, en un plazo no mayor de tres días hábiles de conocer este hecho, deberá comunicar esta circunstancia al registro público, bajo su responsabilidad, y solicitar una anotación preventiva, que tendrá una vigencia de un año contado a partir de la fecha del asiento de presentación. Si dentro de ese plazo, se anota la demanda judicial o medida cautelar que se refiera a este mismo hecho, dicha anotación judicial se correlacionará con la anotación preventiva y surtirá sus efectos desde la fecha del asiento de presentación de esta última. La interposición de estas acciones judiciales, corresponderá a aquellos que tengan interés legítimo en la nulidad de la inscripción obtenida con el título falsificado.

Vencido el plazo de un año a que se refiere el primer párrafo, sino se hubiera anotado la demanda o medida cautelar, la anotación preventiva caduca de pleno derecho. (*)

(*) Disposición modificada por la Tercera Disposición Complementaria Final de la Ley N° 30313, publicada el 26 marzo 2015, cuyo texto es el siguiente:

“ Quinta.- En el caso de inscripciones sustentadas en instrumentos notariales protocolares o extraprotocolares presumiblemente falsificados, el notario al que supuestamente se atribuye la actuación notarial deberá presentar la solicitud de anotación preventiva en el diario de la oficina registral dentro de los cinco días hábiles contados desde que tuvo conocimiento, bajo su responsabilidad.

Igual procedimiento le corresponde al notario que tome conocimiento de la falsificación de un instrumento protocolar o extraprotocolar que se le atribuya y se haya insertado en instrumento que diera lugar a la inscripción registral.

La presentación posterior a dicho plazo no constituye una causa de inadmisión o improcedencia de la solicitud del notario ante el Registro.

La anotación preventiva tendrá la vigencia de un año contado a partir de la fecha del asiento de presentación. Si dentro de ese plazo, se anota la demanda judicial o medida cautelar que se refiera a este mismo hecho, dicha anotación judicial se correlacionará con la anotación preventiva y surtirá sus efectos desde la fecha del asiento de presentación de esta última. La interposición de estas acciones judiciales, corresponderá a aquellos que tengan interés legítimo en la nulidad de la inscripción obtenida con el título falsificado.

Vencido el plazo de la anotación preventiva que fuera solicitada por el notario, si no se hubiera anotado la demanda o medida cautelar, dicha anotación preventiva caduca de pleno derecho.

La presente anotación preventiva será procedente aunque el actual titular registral sea un tercero distinto al que adquirió un derecho sobre la base del instrumento notarial presuntamente falsificado” .

CONCORDANCIAS:      R. N° 315-2013-SUNARP-SN, Art 1.

Sexta.- En el caso de inscripciones sustentadas en escrituras públicas en las que presumiblemente se habría suplantado al o a los otorgantes, el Notario ante quien se otorgó dicho instrumento, podrá solicitar al Registro Público, bajo su responsabilidad, una anotación preventiva, que tendrá una vigencia de un año contado a partir de la fecha del asiento de presentación. Si dentro de ese plazo, se anota la demanda judicial o medida cautelar que se refiera a este mismo hecho, dicha anotación judicial se correlacionará con la anotación preventiva y surtirá sus efectos desde la fecha del asiento de presentación de esta última. La interposición de estas acciones judiciales, corresponderá a aquellos que tengan interés legítimo en la nulidad de la inscripción obtenida con el título falsificado.

Vencido el plazo de un año a que se refiere el primer párrafo, sin que se hayan anotado la demanda o medida cautelar, la anotación preventiva caduca de pleno derecho.

En lo que resulte aplicable, las disposiciones complementarias primera y segunda se regirán por las disposiciones contenidas en el Reglamento General de los Registros Públicos.(*)

(*) Disposición modificada por la Tercera Disposición Complementaria Final de la Ley N° 30313, publicada el 26 marzo 2015, cuyo texto es el siguiente:

“ Sexta.- En el caso de inscripciones sustentadas en instrumentos públicos protocolares en las que presumiblemente se habría suplantado al o a los otorgantes, o a sus respectivos representantes, el notario ante quien se otorgó dicho instrumento debe presentar la solicitud de anotación preventiva en el diario de la oficina registral, dentro de los cinco días hábiles contados desde que tuvo conocimiento, bajo su responsabilidad.

La presentación posterior a dicho plazo no constituye una causa de inadmisión o improcedencia de la solicitud del notario ante el Registro.

La anotación preventiva tendrá la vigencia de un año contado a partir de la fecha del asiento de presentación. Si dentro de ese plazo, se anota la demanda judicial o medida cautelar que se refiera a este mismo hecho, dicha anotación judicial se correlacionará con la anotación preventiva y surtirá sus efectos desde la fecha del asiento de presentación de esta última. La interposición de estas acciones judiciales, corresponderá a aquellos que tengan interés legítimo en la nulidad de la inscripción obtenida con el título falsificado.

Vencido el plazo de la anotación preventiva que fuera solicitada por el notario, si no se hubiera anotado la demanda o medida cautelar, dicha anotación preventiva caduca de pleno derecho.

La presente anotación preventiva será procedente aunque el actual titular registral sea un tercero distinto al que adquirió un derecho sobre la base del instrumento notarial sujeto a la presunta falsificación.

En lo que resulte aplicable, las disposiciones complementarias quinta y sexta de las disposiciones complementarias, transitorias y finales del Decreto Legislativo 1049, Decreto Legislativo del Notariado, se regirán por las disposiciones contenidas en el Texto Único Ordenado del Reglamento General de los Registros Públicos” .

CONCORDANCIAS:      R. N° 315-2013-SUNARP-SN, Art 1.

Sétima.- La presentación de partes notariales a los Registros de Predios, de Mandatos y Poderes en las oficinas regístrales, deberá ser efectuada por el notario ante quien se otorgó el instrumento o por sus dependientes acreditados.

Luego de la presentación, el notario podrá entregar la guía de presentación a los interesados a fin de que éste continúe la tramitación de la inscripción, bajo su responsabilidad.

Excepcionalmente, a solicitud y bajo responsabilidad de los otorgantes, los partes notariales podrán ser presentados y tramitados por persona distinta al notario o sus dependientes. En este caso, el notario al expedir el parte deberá consignar en este el nombre completo y número de documento de identidad de la persona que se encargará de la presentación y tramitación de dicho parte y la procedencia legitima del parte.

La oficina registral ante la cual se presente el título verificará, bajo responsabilidad, que el presentante sea la persona señalada en el parte notarial y la debida procedencia.

Las oficinas regístrales en estos casos no admitirán, bajo responsabilidad, la presentación de testimonios y títulos regístrales.(*) (*) RECTIFICADO POR FE DE ERRATAS

(*) Disposición modificada por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Sétima.- La presentación de partes notariales y copias certificadas en los distintos registros del Sistema Nacional de los Registros Públicos, según corresponda, deberá ser efectuada por el notario o por sus dependientes acreditados ante la SUNARP.

Luego de la presentación, el notario podrá entregar la solicitud de inscripción del título al interesado para que éste continúe la tramitación del procedimiento, bajo su responsabilidad.

Excepcionalmente, a solicitud y bajo responsabilidad del interesado, los partes notariales y las copias certificadas podrán ser presentados y tramitados por persona distinta al notario o sus dependientes. El notario al expedir el parte o la copia certificada deberá consignar en estos instrumentos el nombre completo y número de documento de identidad de la persona que se encargará de la presentación y tramitación.

Para la presentación de los instrumentos ante el Registro, el notario acreditará a su dependiente a través del módulo “Sistema Notario” que administra la Superintendencia Nacional de los Registros Públicos - SUNARP. Tratándose de la excepción prevista en el párrafo precedente, el notario incorporará en el Módulo “Sistema Notario” los datos de la persona distinta que presentará el parte notarial o la copia certificada.

Las oficinas regístrales no admitirán, bajo responsabilidad, la presentación de testimonios o boletas notariales” .

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 36

Octava.- Deróguese el Decreto Ley Nº 26002 - Ley del Notariado y sus normas modificatorias y complementarias, así como todas las normas que se opongan a lo dispuesto en el presente Decreto Legislativo.

Novena.- El presente dispositivo legal entrará en vigencia a partir del día siguiente de su publicación, con excepción del inciso b) de su artículo 21 que entrará en vigencia a partir del primero de enero del 2014.

“ Décima.- Para la identificación de los extranjeros residentes o no en el país a que se refiere el artículo 55 de la presente ley, la Superintendencia Nacional de Migraciones deberá poner a disposición de los notarios el acceso a la información de la base de datos del registro de carnés de extranjería, pasaportes y control migratorio de ingreso de extranjeros, en el plazo de ciento ochenta (180) días calendario, contados desde la vigencia del presente Decreto Legislativo” .(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Décimo Primera.- El módulo denominado “Sistema Notario” aprobado por la Superintendencia Nacional de los Registros Públicos - SUNARP es de uso obligatorio para los notarios. El notario deberá incorporar, modificar o eliminar la información que se encuentre habilitada en el mencionado sistema para coadyuvar a contrarrestar el riesgo de la presentación de documentos notariales falsificados.

Serán rechazados por el diario de la oficina registral la presentación de títulos realizados por el notario, su dependiente acreditado, o por persona distinta que no hayan sido incorporados en el módulo “Sistema Notario”.

Cuando el notario no tenga las facilidades tecnológicas, el Jefe de la Unidad Registral de la Zona Registral del ámbito geográfico correspondiente al domicilio notarial orientará sobre el empleo del módulo “Sistema Notario”, dándole las facilidades a fin de acudir a la Oficina Registral para acceder a Internet.

La información de los dependientes de notaría que fueron acreditados ante el Registro con la presentación de una solicitud en soporte papel sólo tendrá eficacia por el plazo de noventa (90) días calendarios contados desde el día siguiente de la publicación en el Diario Oficial El Peruano de la presente disposición."(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Décimo Segunda.- Mediante Decreto Supremo se aprobará el uso de medios informáticos que permitan verificar de manera fehaciente la autenticidad de los instrumentos notariales presentados a los registros a cargo de la Superintendencia Nacional de los Registros Públicos - SUNARP. En tal caso, se integrará su utilización al módulo “sistema notario” señalado en la presente ley."(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Décimo Tercera.- A partir del primero de febrero de 2016, los partes notariales que contengan actos inscribibles en el Registro de Mandatos y Poderes de la Oficina Registral de Lima de la Zona Registral Nº IX - Sede Lima, se expedirán en formato digital utilizando la tecnología de firmas y certificados digitales de acuerdo a la ley de la materia, y se presentarán a través de la plataforma informática administrada por la Superintendencia Nacional de los Registros Públicos - SUNARP.

Para estos efectos, la oficina registral de Lima de la Zona Registral Nº IX - Sede Lima no admitirá, bajo responsabilidad, la presentación del parte notarial en soporte papel a partir de la entrada en vigencia de la presente disposición.

Mediante Resolución del Superintendente Nacional de los Registros Públicos se determinará la obligación de presentar los partes notariales utilizando la tecnología de firmas y certificados digitales para actos inscribibles en otros registros, así como en las Zonas Registrales correspondientes."(*)(**)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

(**) De conformidad con el Artículo 1 de la Resolución N° 064-2020-SUNARP-SN, publicada el 03 junio 2020, se dispone que, a partir del 08 de junio de 2020, los partes notariales conteniendo actos a inscribirse en el Registro de Mandatos y Poderes de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

(**) De conformidad con el Artículo 2 de la Resolución N° 064-2020-SUNARP-SN, publicada el 03 junio 2020, se dispone que, a partir del 10 de junio de 2020, los partes notariales conteniendo el acto constitutivo de Sociedades Anónimas, Sociedades Anónimas Cerradas, Sociedades Comerciales de Responsabilidad Limitada y Empresas Individuales de Responsabilidad Limitada a inscribirse en el Registro de Personas Jurídicas de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

(**) De conformidad con el Artículo 3 de la Resolución N° 064-2020-SUNARP-SN, publicada el 03 junio 2020, se dispone que, a partir del 15 de junio de 2020, los partes notariales conteniendo los actos de compraventa, donación, dación en pago, anticipo de legítima y permuta de vehículo a inscribirse en el Registro de Propiedad Vehicular de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

(**) De conformidad con el Artículo 1 de la Resolución N° 200-2020-SUNARP-SN, publicada el 31 diciembre 2020, se dispone que, a partir del 04 de enero de 2021, las solicitudes de anotación y levantamiento de Bloqueo Registral a inscribirse en el Registro de Predios de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

(**) De conformidad con el Artículo 2 de la Resolución N° 200-2020-SUNARP-SN, publicada el 31 diciembre 2020, se dispone que, a partir del 11 de enero de 2021, las copias certificadas o partes notariales sobre nombramiento, remoción o renuncia de gerente general y otorgamiento o revocatoria de apoderado de Sociedad Anónima (S.A), de Sociedad Anónima Cerrada (S.A.C), de Sociedad Comercial de Responsabilidad Limitada (S.R.L) y de Empresa Individual de Responsabilidad Limitada (E.I.R.L), a inscribirse en el Registro de Personas Jurídicas de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

(**) De conformidad con el Artículo 3 de la Resolución N° 200-2020-SUNARP-SN, publicada el 31 diciembre 2020, se dispone que, a partir del 18 de enero de 2021, las solicitudes de anotación preventiva de sucesión intestada, de su levantamiento o inscripción definitiva, a inscribirse en el Registro de Sucesiones Intestadas de todas las Oficinas Registrales del país, se expidan con firma digital y se tramiten exclusivamente a través del Sistema de Intermediación Digital de la SUNARP (SID-SUNARP), en aplicación de la presente disposición.

CONCORDANCIAS:      R.N° 212-2017-SUNARP-SN (Disponen que los partes notariales que contengan actos inscribibles en el Registro de Mandatos y Poderes de la Oficina Registral del Callao de la Zona Registral Nº IX - Sede Lima, se expidan con firma digital para ser presentados al registro mediante el Sistema de Intermediación Digital - SID SUNARP)

R.N° 184-2019-SUNARP-SN (Disponen que la presentación de partes notariales que contengan actos inscribibles en el registro de Mandatos y Poderes de las Oficinas Registrales de Huancayo, Huánuco, Huancavelica, Tarma, Pasco, Tingo María, Selva Central y Satipo correspondientes a la Zona Registral Nº VIII, se efectúen mediante firma digital del notario a través del Sistema de Intermediación Digital de la Sunarp (SID-SUNARP))

“ Décimo Cuarta.- El papel notarial de seguridad para la expedición del parte, a que se refiere el artículo 85 de la presente Ley, deberá ser de uso uniforme a nivel nacional y de aplicación a partir del 1 de abril de 2016. La Junta de Decanos del Colegios de Notarios del Perú determinará las características especiales del papel notarial de seguridad y demás acciones necesarias destinadas a su implementación."(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Décimo Quinta.- El Consejo del Notariado puede disponer los traslados temporales de notarios a nivel nacional, en los siguientes supuestos:

a) Cuando existan plazas vacantes y hasta que sean cubiertas en virtud de un concurso público de méritos.

b) Si el concurso público es declarado desierto, hasta que se cubran las plazas por concursos públicos regulares.

Asimismo, el Consejo del Notariado podrá disponer el cese del traslado por razones de necesidad debidamente sustentadas."(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Décimo Sexta.- Déjese sin efecto la Sétima Disposición Complementaria Transitoria de la Ley Nº 29933.

La convocatoria a concursos públicos de méritos para el ingreso a la función notarial se realizará únicamente para las plazas notariales que no se encuentren comprendidas dentro de los alcances de la Segunda Disposición Complementaria Transitoria hasta que se declare concluido el concurso público nacional de méritos para el ingreso a la función notarial.

Sólo se podrá postular a un concurso público de méritos por vez. En caso que un participante decida postular a un concurso público de méritos para el ingreso a la función notarial distinto al que se encuentre inscrito deberá formular desistimiento."(*)

(*) Disposición incorporada por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

POR TANTO

Mando se publique y cumpla, dando cuenta al Congreso de la República.

Dado en la Casa de Gobierno, en Lima, a los veinticinco días del mes de junio del año dos mil ocho.

ALAN GARCÍA PÉREZ

Presidente Constitucional de la República

JORGE DEL CASTILLO GÁLVEZ

Presidente del Consejo de Ministros

ROSARIO DEL PILAR FERNÁNDEZ FIGUEROA

Ministra de Justicia
