#####################################
CAPÍTULO II DEL RÉGIMEN DISCIPLINARIO
#####################################

Artículo 147.- Competencia Disciplinaria
########################################

La disciplina del notariado es competencia del Consejo del Notariado y el Tribunal de Honor de los colegios de notarios.

Contra las resoluciones del Tribunal de Honor de los colegios de notarios sólo procede recurso de apelación. Las resoluciones del Consejo del Notariado, agotan la vía administrativa.

CONCORDANCIAS :      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arts. 64, 72 y 73

Artículo 148.- Garantías del Proceso
####################################

En todo proceso disciplinario se garantizará el derecho de defensa del notario, así como todos los derechos y garantías inherentes al debido procedimiento, que comprende el derecho a exponer sus argumentos, a ofrecer y producir pruebas y a obtener una decisión motivada y fundada en derecho.
