###############################################################
CAPÍTULO III DE LAS INFRACCIONES ADMINISTRATIVAS DISCIPLINARIAS
###############################################################

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arst. 74, 75 y 76

Artículo 149.- Infracciones Disciplinarias
##########################################

Constituyen infracciones administrativas disciplinarias las siguientes:

a) La conducta no acorde con la dignidad y decoro del cargo.

b) Cometer hecho grave que sin ser delito lo desmerezca en el concepto público.

c) El incumplimiento de los deberes y obligaciones del notario establecidos en esta ley, normas reglamentarias y/o conexas, Estatuto y Código de Ética.

d) El no acatar las prohibiciones contempladas en esta ley, normas reglamentarias y/o conexas, Estatuto y Código de Ética.

e) La embriaguez habitual y/o el uso reiterado e injustificado de sustancias alucinógenas o fármaco dependientes.

f) El continuo incumplimiento de sus obligaciones civiles, comerciales y tributarias.

g) Agredir física y/o verbalmente, así como faltar el respeto a los notarios, miembros de la junta directiva, tribunal de honor y/o Consejo del Notariado.

h) El ofrecer dádivas para captar clientela; y,

i) El aceptar o solicitar honorarios extras u otros beneficios, para la realización de actuaciones irregulares.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 149.- Infracciones Disciplinarias

Las infracciones disciplinarias se clasifican en muy graves, graves y leves, las cuales serán sancionadas conforme a lo previsto en el artículo 150 de la presente ley.” (*) RECTIFICADO POR FE DE ERRATAS

CONCORDANCIAS:      D.S. Nº 006-2013-JUS (Decreto Supremo que establece limitaciones para la realización de transacciones en efectivo dentro de los oficios notariales, así como la                 obligatoriedad del uso del sistema de verificación de la identidad por comparación biométrica), Art. 12

“ Artículo 149-A.- Infracciones Disciplinarias Muy Graves

Son infracciones disciplinarias muy graves:

a) La comisión de infracciones disciplinarias graves cometidas tres (03) veces dentro del plazo de un (01) año y siempre que las resoluciones sancionadoras hayan quedado firmes.

b) El uso indebido de la firma digital, el incumplimiento de la obligación de custodia, la omisión de denunciar la pérdida, extravío, deterioro o situación que ponga en riesgo el secreto o la unidad del dispositivo de creación de firma digital.

c) Aceptar o solicitar honorarios u otros beneficios para la realización de actuaciones irregulares.

d) Efectuar declaraciones y juicios, en la extensión de los instrumentos notariales, cuando le conste la falsedad de los actos, hechos o circunstancias materia de dichos instrumentos.

e) Negar dolosamente la existencia de un instrumento protocolar de su oficio notarial.

f) Destruir dolosamente un instrumento protocolar.

g) Tener más de un oficio notarial.

h) La falta de cierre o la reapertura indebida del oficio notarial, por parte del notario suspendido por medida disciplinaria o medida cautelar.

i) Ejercer función respecto a asuntos o procedimientos que no están previstos dentro de la competencia del Notario.

j) Expedir, dolosamente traslados instrumentales, alterando datos esenciales del instrumento o respecto a instrumentos inexistentes.

k) La embriaguez habitual y/o el uso reiterado e injustificado de sustancias alucinógenas o farmacológicas que generen dependencia.

l) Dar fe de capacidad cuando el compareciente sea notoriamente incapaz al momento de otorgar el instrumento.

m) Incumplir dolosamente y causando perjuicio a tercero, cualquier deber propio de la función notarial, ya sea de origen legal, reglamentario o estatutario.

n) La responsabilidad funcional a que se refiere el literal d) del artículo 55 de la presente Ley.

o) Desempeñar cargos, labores o representaciones a los que está prohibido según la presente Ley.

p) Ejercer la abogacía, salvo en las excepciones previstas en la normatividad vigente.

q) Delegar en forma total o parcial sus funciones.

r) Incumplir dolosamente cualquier deber propio de la función notarial, ya sea de origen legal, reglamentario o estatutario; causando perjuicios a terceros.

s) Las demás infracciones aprobadas mediante el reglamento del Decreto Legislativo Nº 1049” .(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Artículo 149-B.- Infracciones Disciplinarias Graves

S on infracciones disciplinarias graves:

a) La comisión de infracciones disciplinarias leves cometidas tres (03) veces dentro del plazo de un (01) año y siempre que las resoluciones sancionadoras hayan quedado firmes.

b) Ejercer su función fuera del ámbito de su competencia territorial.

c) No desagregue en los comprobantes de pago los servicios en línea que brinda el Registro Nacional de Identificación y Estado Civil - RENIEC, cobrando al usuario más de lo que esta entidad fija por el servicio.

d) No devolver al usuario el monto en exceso que se haya cobrado por los servicios registrales brindados por la Superintendencia Nacional de Registros Públicos.

e) Extender instrumentos notariales declarando actos, hechos o circunstancias cuya realización y veracidad no le consten, siempre que ellos sean materia de verificación por el notario.

f) Incumplir con sus obligaciones tributarias durante un periodo de dos (2) años consecutivos.

g) Realizar declaración dentro de un procedimiento no contencioso invocando la existencia de pruebas que no consten en el expediente, así como incumplir las obligaciones legales y reglamentarias de responsabilidad del notario, aplicables a dicho procedimiento.

h) Omitir los procedimientos establecidos en los literales a), b) y c) del artículo 55 de la presente ley, salvo la excepción contemplada en el literal d) del citado artículo.

i) Negarse a las visitas de inspección ordinaria, o las extraordinarias que disponga su Colegio, el Tribunal de Honor y/o el Consejo del Notariado.

j) Agresión física, verbal o por escrito a notarios, miembros del Tribunal de Honor, de la Junta Directiva y/o del Consejo del Notariado.

k) Ofrecer dádivas para captar clientela.

l) Cometer hecho grave que sin ser delito, lo desmerezca en el concepto público por afectar la moral, la ética y/o el orden público. No están comprendidas dentro de dichas conductas la expresión de preferencias o creencias que constituyen el legítimo ejercicio de sus derechos constitucionalmente protegidos.

m) No actualizar sus datos en el Registro Nacional de Notarios.

n) Violar el secreto profesional.

o) Negar sin dolo la existencia de un instrumento protocolar de su oficio notarial.

p) Incumplir injustificada y reiteradamente los mandatos procedentes del órgano judicial y del Ministerio Público.

q) Incumplir dolosamente cualquier deber propio de la función notarial, ya sea de origen legal, reglamentario o estatutario.

r) No realizar las comunicaciones a los colegios de notarios y al Consejo del Notariado que la Ley y su reglamento imponen.

s) No proteger adecuadamente la documentación que se encuentra comprendida dentro del ámbito del secreto profesional.

t) Incumplir las disposiciones emitidas por el Consejo del Notariado.

u) Las demás infracciones aprobadas mediante el reglamento del Decreto Legislativo Nº 1049” .(*) (*) RECTIFICADO POR FE DE ERRATAS

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

“ Artículo 149-C.- Infracciones Disciplinarias Leves

S on infracciones disciplinarias leves:

a) Retardo notorio e injustificado en la extensión de un instrumento o en la expedición de un traslado.

b) No emplear la debida diligencia en la extensión de instrumentos notariales o en la expedición de traslados instrumentales.

c) No adoptar los medios idóneos que garanticen la adecuada conservación de los documentos que conforman su archivo.

d) No cumplir con los requisitos mínimos de capacitación establecidos en la normativa aplicable.

e) No cumplir con el horario mínimo señalado en la Ley.

f) No responder de manera oportuna a las comunicaciones formuladas por las instancias registrales sobre la autenticidad de los instrumentos notariales.

g) Incumplir injustificadamente los encargos o comisiones que se le encomiende en el ejercicio de su función, incluyendo las obligaciones que respecto a la supervisión de la función notarial le correspondan en caso de asumir cargo directivo en su colegio.

h) No mantener una infraestructura física y/o tecnológica mínima de acuerdo a lo establecido por la presente Ley y su Reglamento.

i) No efectuar debidamente las verificaciones necesarias y el exacto diligenciamiento, según corresponda, en la autorización de actas y certificaciones.

j) No brindar sus servicios en los términos y oportunidad ofrecidos.

k) Faltar el respeto de cualquier modo a notarios, miembros del Tribunal de Honor, de la Junta Directiva y/o del Consejo del Notariado.

l) Usar publicidad que contravenga lo dispuesto en la presente Ley, su Reglamento o en normas de carácter especial en materia de publicidad.

m) Incumplir sin dolo cualquier otro deber propio de la función notarial, ya sea de origen legal, reglamentario o estatutario.

n) Las demás infracciones aprobadas mediante el reglamento del Decreto Legislativo Nº 1049” .(*) (*) RECTIFICADO POR FE DE ERRATAS

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.
