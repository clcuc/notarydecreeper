####################################################################
CAPÍTULO IV DE LAS SANCIONES, DEL PROCEDIMIENTO Y LA MEDIDA CAUTELAR
####################################################################

Artículo 150.- Tipos de Sanciones
#################################

Las sanciones que pueden aplicarse en el procedimiento disciplinario son:

a) Amonestación privada.

b) Amonestación pública.

c) Suspensión temporal del notario del ejercicio de la función hasta por un máximo de un año.

d) Destitución.

Las sanciones se aplicarán sin necesidad de seguir la prelación precedente, según la gravedad del daño al interés público y/o el bien jurídico protegido. Adicionalmente podrá tenerse en cuenta la existencia o no de intencionalidad en la conducta del infractor, la repetición y/o continuidad en la comisión de la infracción y/o el perjuicio causado.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 150.- Tipos de Sanciones

Las sanciones que pueden aplicarse en el procedimiento disciplinario son:

a) En caso de infracciones disciplinarias leves: la amonestación privada o la amonestación pública y una multa no mayor a una (1) UIT.

b) En caso de infracciones disciplinarias graves: la suspensión temporal del notario del ejercicio de la función hasta por un máximo de un (01) año y una multa no mayor a diez (10) UIT.

c) En caso de infracciones disciplinarias muy graves: la destitución y una multa mayor de 10 UIT y hasta 20 UIT.

La multa impuesta será destinada a favor del órgano que impone la misma” .

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 77

D.S. Nº 006-2013-JUS (Decreto Supremo que establece limitaciones para la realización de transacciones en efectivo dentro de los oficios notariales, así como la                 obligatoriedad del uso del sistema de verificación de la identidad por comparación biométrica), Art. 12

Artículo 151.- Del inicio del Proceso Disciplinario
###################################################

La apertura de procedimiento disciplinario corresponde al Tribunal de Honor del colegio de notarios mediante resolución de oficio, bien por propia iniciativa, a solicitud de la junta directiva, del Consejo del Notariado, o por denuncia. En este último caso, el Tribunal de Honor previamente solicitará informe al notario cuestionado a fin que efectúe su descargo en un plazo máximo de 10 días hábiles y en mérito de éste el Tribunal de Honor resolverá si hay lugar a iniciar proceso disciplinario en un plazo máximo de 20 días hábiles.

La resolución que dispone abrir procedimiento disciplinario es inimpugnable, debiendo inmediatamente el Tribunal de Honor remitir todo lo actuado al Fiscal del Colegio respectivo a fin que asuma la investigación de la presunta infracción administrativa disciplinaria.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 151.- Inicio del Proceso Disciplinario

La apertura de procedimiento disciplinario corresponde al Tribunal de Honor del colegio de notarios mediante resolución de oficio, bien por propia iniciativa, a solicitud de la junta directiva, del Consejo del Notariado, o por denuncia. En este último caso, el Tribunal de Honor previamente solicitará informe al notario cuestionado a fin que efectúe su descargo en un plazo máximo de diez (10) días hábiles y en mérito de éste el Tribunal de Honor resolverá si hay lugar a iniciar proceso disciplinario en un plazo máximo de veinte (20) días hábiles.

Cuando el procedimiento disciplinario se inicia a solicitud del Tribunal de Honor o de la junta directiva del colegio de notarios o del Consejo del Notariado, se abrirá investigación sin previa calificación.

La resolución que dispone abrir procedimiento disciplinario es inimpugnable, debiendo inmediatamente el Tribunal de Honor remitir todo lo actuado al Fiscal del Colegio respectivo a fin que asuma la investigación de la presunta infracción administrativa disciplinaria” .

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento). Arts. 66, 67, 68, 69,70, 71

Artículo 152.- Proceso Disciplinario
####################################

En primera instancia, el proceso disciplinario se desarrollará en un plazo máximo de noventa (90) días hábiles, siendo los primeros cuarenta (45) días hábiles para la investigación a cargo del Fiscal, quien deberá emitir dictamen con la motivación fáctica y jurídica de opinión por la absolución o no del procesado y de ser el caso, la propuesta de sanción procediendo inmediatamente a devolver todo lo actuado al Tribunal de Honor para su resolución.

En caso que, el Fiscal haya emitido dictamen de opinión por la responsabilidad del procesado y el Tribunal de Honor hubiera resuelto por la absolución o sanción menor a la propuesta, el Fiscal está obligado a interponer el recurso de apelación.

En segunda instancia el plazo no excederá de ciento ochenta (180) días hábiles.

Los plazos establecidos para el procedimiento disciplinario no son de caducidad, pero su incumplimiento genera responsabilidad para las autoridades competentes.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 152.- Proceso Disciplinario

En primera instancia, el proceso disciplinario se desarrollará en un plazo máximo de noventa (90) días hábiles, siendo los primeros cuarenta y cinco (45) días hábiles para la investigación a cargo del Fiscal, quien deberá emitir dictamen con la motivación fáctica y jurídica de opinión por la absolución o no del procesado y de ser el caso, la propuesta de sanción procediendo inmediatamente a devolver todo lo actuado al Tribunal de Honor para su resolución. Excepcionalmente y tratándose de casos complejos, debidamente sustentados y demostrados, podrá ampliarse el plazo en treinta (30) días hábiles adicionales, máximo en dos (2) oportunidades.

En caso que, el Fiscal haya emitido dictamen de opinión por la responsabilidad del procesado y el Tribunal de Honor hubiera resuelto por la absolución o sanción menor a la propuesta, el Fiscal está obligado a interponer el recurso de apelación.

En segunda instancia el plazo no excederá de ciento ochenta (180) días hábiles.

Los plazos establecidos para el procedimiento disciplinario no son de caducidad, pero su incumplimiento genera responsabilidad para las autoridades competentes. En el caso del Tribunal de Honor, si se incumple con el plazo establecido en el presente artículo, se aplicará a cada uno de sus miembros, una sanción del 0.5 de una (01) Unidad Impositiva Tributaria, la misma que continuará devengándose por el mismo monto por cada seis (6) meses mientras subsista el incumplimiento. Esta sanción se aplica por cada procedimiento disciplinario. El titular de la multa es el Consejo del Notariado. Las resoluciones finales emitidas en primera instancia en los procedimientos iniciados de oficio, serán remitidas en revisión al Consejo del Notariado.”

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Arts. 65 y 70

Artículo 153.- Medida Cautelar
##############################

Mediante decisión motivada, de oficio o a solicitud del colegio respectivo o del Consejo del Notariado, el Tribunal de Honor de los colegios de notarios al inicio del procedimiento disciplinario podrá disponer como medida cautelar la suspensión del notario procesado en caso de existir indicios razonables de la comisión de infracción administrativa disciplinaria y dada la gravedad de la conducta irregular, se prevea la imposición de la sanción de destitución. Dicha decisión será comunicada a la junta directiva del colegio respectivo, a fin que proceda al cierre de los registros y la designación del notario que se encargue del oficio en tanto dure la suspensión. En ningún caso la medida cautelar podrá exceder el plazo máximo fijado por la presente ley para el desarrollo del procedimiento disciplinario, bajo responsabilidad de la autoridad competente.

El recurso de apelación no suspende la medida cautelar.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 78
