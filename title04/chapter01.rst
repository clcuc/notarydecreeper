##############################################################
CAPÍTULO I DE LA RESPONSABILIDAD EN EL EJERCICIO DE LA FUNCIÓN
##############################################################

Artículo 144.- Definición
#########################

El notario tiene responsabilidad administrativa disciplinaria por el incumplimiento de esta ley, normas conexas y reglamentarias, estatuto y decisiones dictadas por el Consejo del Notariado y colegio de notarios respectivo.

Artículo 145.- Responsabilidades
################################

El notario es responsable, civil y penalmente, de los daños y perjuicios que, por dolo o culpa, ocasione a las partes o terceros en el ejercicio de la función.

Artículo 146.- Autonomía de Responsabilidad
###########################################

Las consecuencias civiles, administrativas o penales de la responsabilidad del notario son independientes y se exigen de acuerdo a lo previsto en su respectiva legislación.
