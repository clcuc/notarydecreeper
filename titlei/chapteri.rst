##################################
CAPÍTULO I DISPOSICIONES GENERALES
##################################

Artículo 1.- Integración del Notariado
######################################

El notariado de la República se integra por los notarios con las funciones, atribuciones y obligaciones que la presente ley y su reglamento señalan.

Las autoridades deberán prestar las facilidades y garantías para el cumplimiento de la función notarial.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 3

Artículo 2.- El Notario
#######################

El notario es el profesional del derecho que está autorizado para dar fe de los actos y contratos que ante él se celebran. Para ello formaliza la voluntad de los otorgantes, redactando los instrumentos a los que confiere autenticidad, conserva los originales y expide los traslados correspondientes.

Su función también comprende la comprobación de hechos y la tramitación de asuntos no contenciosos previstos en las leyes de la materia.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 4

Artículo 3.- Ejercicio de la Función Notarial
#############################################

El notario ejerce su función en forma personal, autónoma, exclusiva e imparcial.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 3.- Ejercicio de la Función Notarial

El notario ejerce su función en forma personal, autónoma, exclusiva e imparcial.

El ejercicio personal de la función notarial no excluye la colaboración de dependientes del despacho notarial para realizar actos complementarios o conexos que coadyuven a su desarrollo, manteniéndose la responsabilidad exclusiva del notario” .

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 6

Artículo 4.- Ámbito territorial
###############################

El ámbito territorial del ejercicio de la función notarial es provincial no obstante la localización distrital que la presente ley determina.(*)

(*) Artículo modificado por la Segunda Disposición Complementaria Final de la Ley N° 30313, publicada el 26 marzo 2015, cuyo texto es el siguiente:

“ Artículo 4.- Ámbito territorial

El ámbito territorial del ejercicio de la función notarial es provincial no obstante la localización distrital que la presente ley determina.

Son nulas de pleno derecho las actuaciones notariales referidas a actos de disposición o gravamen intervivos de bienes inmuebles ubicados fuera del ámbito territorial del notario provincial, sin perjuicio que de oficio se instaure al notario el proceso disciplinario establecido en el Título IV de la presente ley. La presente disposición no se aplica al cónsul cuando realiza funciones notariales.

Cuando el acto de disposición o gravamen comprenda más de un inmueble ubicado en diferentes provincias es competente el notario del lugar donde se encuentre cualquiera de ellos, quedando autorizado para ejercer función notarial fuera de los límites de la provincia para la cual ha sido nombrado” .(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 4.- Ámbito territorial

El ámbito territorial del ejercicio de la función notarial es provincial no obstante la localización distrital que la presente ley determina."

(*) De conformidad con la Primera Disposición Complementaria Transitoria del Decreto Supremo N° 010-2016-JUS, publicado el 23 julio 2016, se dispone que se encuentra excluida de las causales de nulidad la escritura pública otorgada antes de la vigencia del artículo 4 del presente Decreto, modificado por la Ley Nº 30313 y el Decreto Legislativo Nº 1232, y cuyo proceso de conclusión de firmas se efectuó dentro de los alcances de las restricciones previstas en dichas normas. La referida disposición entra en vigencia en el plazo de cuarenta y cinco (45) días hábiles contados a partir del día siguiente de la publicación del citado Decreto Supremo en el Diario Oficial El Peruano, a excepción del Capítulo I y el Subcapítulo IV del Capítulo V que entran en vigencia a partir del día siguiente de dicha publicación.

(*) De conformidad con la Segunda Disposición Complementaria Transitoria del Decreto Supremo N° 010-2016-JUS, publicado el 23 julio 2016, se dispone que las escrituras públicas de otorgamiento de poder se encuentran excluidas de los supuestos de nulidad de escrituras públicas previstas en el artículo 4 del presente Decreto, modificado por la Ley Nº 30313 y el Decreto Legislativo Nº 1232. La referida disposición entra en vigencia en el plazo de cuarenta y cinco (45) días hábiles contados a partir del día siguiente de la publicación del citado Decreto Supremo en el Diario Oficial El Peruano, a excepción del Capítulo I y el Subcapítulo IV del Capítulo V que entran en vigencia a partir del día siguiente de dicha publicación.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 7

Artículo 5.- Creación de Plazas Notariales
##########################################

5.1. El número de notarios en el territorio de la República se establece de la siguiente manera:

a. Una provincia que cuente con al menos cincuenta mil habitantes deberá contar con no menos de dos Notarios.

b. Por cada cincuenta mil habitantes adicionales, se debe contar con un Notario adicional.

5.2. La localización de las plazas son determinados por el Consejo del Notariado. En todo caso, no se puede reducir el número de las plazas existentes.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 5.- Creación de plazas notariales

5.1. El número de notarios en el territorio de la República se establece de la siguiente manera:

a. Una provincia que cuente con al menos cincuenta mil habitantes deberá contar con no menos de dos Notarios.

b. Por cada cincuenta mil habitantes adicionales, se debe contar con un Notario adicional.

c. En función a la magnitud de la actividad económica o tráfico comercial de la provincia.

5.2. La localización de las plazas son determinados por el Consejo del Notariado. En todo caso, no se puede reducir el número de las plazas existentes” .

CONCORDANCIAS:      D.S.Nº 015-2008-JUS, Art. 3

D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 8
