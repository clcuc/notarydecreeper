#############################################
CAPÍTULO II DEL INGRESO A LA FUNCIÓN NOTARIAL
#############################################

Artículo 6.- Ingreso a la Función Notarial
##########################################

El ingreso a la función notarial se efectúa mediante concurso público de méritos ante jurado calificador constituido según lo dispuesto en el artículo 11 de la presente ley.

Las etapas del concurso son: calificación de currículum vitae, examen escrito y examen oral. Cada etapa es eliminatoria e irrevisable.

CONCORDANCIAS:      D.S.Nº 015-2008-JUS, Arts. 1,12,16, 18,19, 22

Artículo 7.- Forma de los Concursos
###################################

Los concursos públicos de méritos para el ingreso a la función notarial serán abiertos y participarán los postulantes que reúnan los requisitos exigidos en el artículo 10 de la presente ley.

En caso que el postulante sea un notario en ejercicio, con una antigüedad no menor de tres (3) años y siempre que en los últimos cinco (5) años no tengan sanciones, tendrá una bonificación máxima del 5% de su nota promedio final.

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Art. 24, 8
D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 7

Artículo 8.- Facultad del Estado
################################

El Estado reconoce, supervisa y garantiza la función notarial en la forma que señala esta ley.

CONCORDANCIAS:       D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 16

Artículo 9.- Convocatoria a Plazas Vacantes
###########################################

Las plazas notariales vacantes o que sean creadas serán convocadas a concurso bajo responsabilidad por los colegios de notarios de la República, por iniciativa propia, en un plazo no mayor de sesenta (60) días calendario de conocer la vacancia o la creación de la plaza.

En el caso de plaza vacante producida por cese de notario, el concurso será convocado en un plazo no mayor de sesenta (60) días calendario de haber quedado firme la resolución de cese.

Asimismo, a requerimiento del Consejo del Notariado, en un plazo no mayor de sesenta (60) días calendario del mismo, los colegios de notarios deberán convocar a concurso para cubrir plazas notariales vacantes o que sean creadas. Transcurrido dicho plazo sin que se convoque a concurso, el Consejo del Notariado quedará facultado a convocarlo.(*)

(*) Párrafo modificado por el Artículo Único de la Ley N° 29933, publicada el 13 noviembre 2012, cuyo texto es el siguiente:

“ Asimismo, a requerimiento del Consejo del Notariado, en un plazo no mayor de treinta (30) días calendario del mismo, los colegios de notarios, bajo responsabilidad de los miembros de la Junta Directiva, deberán convocar a concurso para cubrir plazas notariales vacantes o que sean creadas. Transcurrido dicho plazo, sin que se convoque a concurso, el Consejo del Notariado, bajo responsabilidad, queda facultado a convocarlo. Si no lo hiciere en el plazo de quince (15) días calendario, lo hace el Ministerio de Justicia y Derechos Humanos.” (*)

(*) De conformidad con la Sétima Disposición Complementaria Transitoria de la Ley N° 29933, publicada el 13 noviembre 2012, se suspende temporalmente la vigencia del artículo 9 del presente Decreto Legislativo, a fin de dar cumplimiento a la segunda disposición complementaria transitoria de la citada Ley.

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 9.- Convocatorias a plazas vacantes

Las plazas notariales vacantes o que sean creadas serán convocadas a concurso bajo responsabilidad por los colegios de notarios de la república, por iniciativa propia, en un plazo no mayor de sesenta (60) días calendario de conocer la vacancia o la creación de la plaza.

En el caso de plaza vacante producida por cese de notario, el concurso será convocado en un plazo no mayor de sesenta (60) días calendario de haber quedado firme la resolución de cese.

Asimismo, a requerimiento del Consejo del Notariado, en un plazo no mayor de treinta (30) días calendario del mismo, los colegios de notarios, bajo responsabilidad de los miembros de la Junta Directiva, deberán convocar a concurso para cubrir plazas notariales vacantes o que sean creadas. Transcurrido dicho plazo, sin que se convoque a concurso, el Consejo del Notariado, bajo responsabilidad, queda facultado a convocarlo. Si no lo hiciere en el plazo de quince (15) días calendario, lo hace el Ministerio de Justicia y Derechos Humanos.

El postulante aprobado sólo puede acceder a una plaza en el distrito notarial al que postuló, en el marco del mismo concurso” .

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Arts. 3, 5 y 6

D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 8, inc. mum. 2

Ley N° 29933, Segunda Disp. Compl. Trans.
R.M. N° 0042-2013-JUS (Determinan el monto correspondiente al derecho de participación al Concurso Público Nacional de Méritos para el                 Ingreso a la Función Notarial)

Artículo 10.- Requisitos de los postulantes
###########################################

Para postular al cargo de notario se requiere:

a) Ser peruano de nacimiento.

b) Ser abogado, con una antigüedad no menor de cinco años.

c) Tener capacidad de ejercicio de sus derechos civiles.

d) Tener conducta moral intachable.

e) No haber sido condenado por delito doloso.

f) Estar física y mentalmente apto para el cargo.

g) Acreditar haber aprobado examen psicológico ante institución designada por el Consejo del Notariado. Dicho examen evaluará los rasgos de personalidad, valores del postulante y funciones intelectuales requeridos para la función notarial.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 10.- Requisitos de los postulantes

Para postular al cargo de notario se requiere:

a) Ser peruano de nacimiento.

b) Ser abogado, con una antigüedad no menor de cinco años.

c) Tener capacidad de ejercicio de sus derechos civiles.

d) Conducirse y orientar su conducta personal y profesional hacia los principios y deberes éticos de respeto, probidad, veracidad, transparencia, honestidad, responsabilidad, autenticidad, respeto a las personas y al ordenamiento jurídico.

e) No haber sido destituido de la función pública por resolución administrativa firme.

f) No haber sido condenado por delito doloso.

g) Estar física y mentalmente apto para el cargo.

h) Acreditar haber aprobado examen psicológico ante institución designada por el Consejo del Notariado. Dicho examen evaluará los rasgos de personalidad, valores del postulante y funciones intelectuales requeridos para la función notarial.

Si durante el proceso del concurso se advierte la pérdida de alguno de los requisitos mencionados, el postulante quedará eliminado del proceso. El acuerdo del Jurado Calificador en este aspecto es irrecurrible” .

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Art. 4, 9
R. Nº 001-2012-JUS-CN-P (Dictan disposiciones sobre concursos públicos de méritos que se encuentran en curso y la participación de instituciones                 en la presentación de propuestas técnicas para elaboración de prueba psicológica a candidatos a la función notarial)

Artículo 11.- El Jurado Calificador
###################################

El jurado calificador de cada concurso público de méritos para el ingreso a la función notarial, se integra de la siguiente forma:

a) La persona que designe el Consejo del Notariado, quien lo preside

b) El Decano del colegio de notarios o quien haga sus veces.

c) El Decano del colegio de abogados o quien haga sus veces.

d) Un miembro del colegio de notarios designado por su Junta Directiva.

e) Un miembro del colegio de abogados designado por su Junta Directiva.

En los colegios de notarios dentro de cuya jurisdicción exista más de un colegio de abogados, sus representantes ante el jurado calificador serán nombrados por el colegio de abogados más antiguo.

Los miembros a que se refieren los incisos d) y e) no necesariamente serán integrantes de la junta directiva.

El quórum para la instalación y funcionamiento del jurado es de tres miembros.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 11.- El Jurado Calificador

El jurado calificador de cada concurso público de méritos para el ingreso a la función notarial, se integra de la siguiente forma:

a) Presidente del Consejo del Notariado o su representante, quien lo presidirá.

b) Representante del Ministro de Justicia y Derechos Humanos.

c) Decano del Colegio de Notarios del Distrito Notarial para el que se convoca el concurso.

d) Presidente de la Junta de Decanos de Colegios de Notarios del Perú o su representante.

e) Decano del Colegio de Abogados de la localidad donde se ubica la plaza notarial o su representante, quienes no podrán ostentar título de notario.

En los colegios de notarios dentro de cuya jurisdicción exista más de un colegio de abogados, su representante ante el jurado calificador será nombrado por el colegio de abogados más antiguo.

El quórum para la instalación y funcionamiento del jurado es de tres miembros” .

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Arts. 12,13, 14

Artículo 12.- Expedición de Título
##################################

Concluido el concurso público de méritos de ingreso a la función notarial, el jurado comunicará el resultado al Consejo del Notariado, para la expedición simultánea de las resoluciones ministeriales a todos los postulantes aprobados y la expedición de títulos por el Ministro de Justicia.

En caso de renuncia del concursante ganador antes de la expedición del título, el Consejo del Notariado podrá asignar la plaza vacante al siguiente postulante aprobado, respetando el orden de mérito del correspondiente concurso.

En caso de declararse desierto el concurso público de mérito para el ingreso a la función notarial, el Colegio de Notarios procederá a una nueva convocatoria.

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Arts. 11, 25, 26, 27 y 56
