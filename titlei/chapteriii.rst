#######################################
CAPÍTULO III DE LOS DEBERES DEL NOTARIO
#######################################

Artículo 13.- Incorporación al Colegio de Notarios
##################################################

El notario deberá incorporarse al colegio de notarios dentro de los treinta (30) días de expedido el título, previo juramento o promesa de honor, ante la Junta Directiva. A solicitud del notario dicho plazo podrá ser prorrogado por igual término.

Artículo 14.- Medidas de Seguridad
##################################

El notario registrará en el colegio de notarios su firma, rúbrica, signo, sellos y otras medidas de seguridad que juzgue conveniente o el colegio determine, y que el notario utilizará en el ejercicio de la función. La firma, para ser registrada deberá ofrecer un cierto grado de dificultad.

Asimismo, el notario está obligado a comunicar cualquier cambio y actualizar dicha información en la oportunidad y forma que establezca el respectivo colegio de notarios. Los colegios de notarios deberán velar por la máxima estandarización de los formatos y medios para la remisión de información a que se refiere el presente párrafo.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Arts. 9 y 19

Artículo 15.- Inicio de la Función Notarial
###########################################

El notario iniciará su función dentro de los treinta (30) días, siguientes a su incorporación, prorrogables a su solicitud por única vez, por igual término.

Artículo 16.- Obligaciones del Notario
######################################

El notario está obligado a:

a) Abrir su oficina obligatoriamente en el distrito en el que ha sido localizado y mantener la atención al público no menos de siete horas diarias de lunes a viernes.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS, (TUO del Reglamento), num. 1 del Art. 10, 11

b) Asistir a su oficina, observando el horario señalado, salvo que por razón de su función tenga que cumplirla fuera de ella.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS, (TUO del Reglamento), Art. 10, num. 1

c) Prestar sus servicios profesionales a cuantas personas lo requieran, salvo las excepciones señaladas en la ley, el reglamento y el Código de Ética.

d) Requerir a los intervinientes la presentación del documento nacional de identidad - D.N.I.- y los documentos legalmente establecidos para la identificación de extranjeros, así como los documentos exigibles para la extensión o autorización de instrumentos públicos notariales protocolares y extraprotocolares.(*)

(*) Literal d) quedará modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1236, publicado el 26 septiembre 2015, el mismo que entró en vigencia a los noventa (90) días hábiles de la publicación del Reglamento de la Ley en el Diario Oficial “El Peruano”, salvo disposición legal en contrario, cuyo texto es el siguiente:

" d) Requerir a los intervinientes la presentación del documento nacional de identidad - D.N.I.- y los documentos de identidad o de viaje determinados para la identificación de extranjeros en el territorio nacional, además de la respectiva calidad y categoría migratoria vigentes conforme a la normatividad sobre la materia, así como los documentos exigibles para la extensión o autorización de instrumentos públicos notariales protocolares y extraprotocolares."(*)

(*) Literal d) modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1350, publicado el 07 enero 2017, el mismo que entró en vigencia el 1 de marzo de 2017, cuyo texto es el siguiente:

" d) Requerir a los intervinientes la presentación del documento nacional de identidad - D.N.I.- y los documentos de identidad o de viaje determinados para la identificación de extranjeros en el territorio nacional, además de la respectiva calidad migratoria vigente conforme a la normativa sobre la materia, así como los documentos exigibles para la extensión o autorización de instrumentos públicos notariales protocolares y extraprotocolares." (*)

(*) Literal modificado por la Cuarta Disposición Complementaria Modificatoria del Decreto Legislativo N° 1372, publicado el 02 agosto 2018, cuyo texto es el siguiente:

" d) Requerir a los intervinientes la presentación del documento nacional de identidad - D.N.I. y los documentos de identidad o de viaje determinados para la identificación de extranjeros en el territorio nacional, además de la respectiva calidad migratoria vigente conforme a la normativa sobre la materia, la constancia de presentación de la declaración jurada informativa sobre beneficiario final ante la SUNAT; documento que acredite que el beneficiario final ha cumplido con proporcionar información sobre su identidad a la persona jurídica o ente jurídico, cuando corresponda; así como los documentos exigibles para la extensión o autorización de instrumentos públicos notariales protocolares y extraprotocolares.”

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 10, num. 2

D.S.N° 003-2019-EF, Art.10 (De las obligaciones del notario)

e) Guardar el secreto profesional.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 10, num. 3

f) Cumplir con esta ley y su reglamento. Asimismo, cumplir con las directivas, resoluciones, requerimientos, comisiones y responsabilidades que el Consejo del Notariado y el colegio de notarios le asignen.

g) Acreditar ante su colegio una capacitación permanente acorde con la función que desempeña.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 10, num. 4

h) Contar con una infraestructura física mínima, que permita una óptima conservación de los instrumentos protocolares y el archivo notarial, así como una adecuada prestación de servicios.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 10, num. 5

i) Contar con una infraestructura tecnológica mínima que permita la interconexión con su colegio de notarios, la informatización que facilite la prestación de servicios notariales de intercambio comercial nacional e internacional y de gobierno electrónico seguro.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 10, num. 5

j) Orientar su accionar profesional y personal de acuerdo a los principios de veracidad, honorabilidad, objetividad, imparcialidad, diligencia, respeto a la dignidad de los derechos de las personas, la constitución y las leyes.

k) Guardar moderación en sus intervenciones verbales o escritas con los demás miembros de la orden y ante las juntas directivas de los colegios de notarios, el Consejo del Notariado, la Junta de Decanos de los Colegios de Notarios del Perú y la Unión Internacional del Notariado Latino.

l) Proporcionar de manera actualizada y permanente de preferencia por vía telemática o en medios magnéticos los datos e información que le soliciten su colegio y el Consejo del Notariado. Asimismo suministrar información que los diferentes poderes del Estado pudieran requerir y siempre que no se encuentren prohibidos por ley.

m) Otorgar todas las facilidades que dentro de la ley pueda brindar a la inversión nacional y extranjera en el ejercicio de sus funciones.

n) Cumplir con las funciones que le correspondan en caso de asumir cargos directivos institucionales; y,

CONCORDANCIAS:      D.S. N° 015-2008-JUS, Art. 3

ñ) Aceptar y brindar las facilidades para las visitas de inspección que disponga tanto su Colegio de Notarios, el Tribunal de Honor y el Consejo del Notariado en el correspondiente oficio notarial.

" o) Aceptar y brindar las facilidades para las visitas de inspección que disponga tanto su Colegio de Notarios, el Tribunal de Honor y el Consejo del Notariado en el correspondiente oficio notarial, así como la Unidad de Inteligencia Financiera."(*)

(*) Inciso incorporado por la Sexta Disposición Complementaria Modificatoria del Decreto Legislativo Nº 1106, publicado el 19 abril 2012.

" p) Cumplir con todas las normas pertinentes en materia de prevención del lavado de activos y del financiamiento del terrorismo, conforme a la legislación de la materia.” (*)(**)

(*) Inciso incorporado por la Sexta Disposición Complementaria Modificatoria del Decreto Legislativo Nº 1106, publicado el 19 abril 2012.

(**) Literal modificado por la Cuarta Disposición Complementaria Modificatoria del Decreto Legislativo N° 1372, publicado el 02 agosto 2018, cuyo texto es el siguiente:

" p) Cumplir con todas las normas pertinentes en materia de prevención del lavado de activos y del financiamiento del terrorismo, conforme a la legislación de la materia; entre estas la identificación del beneficiario final en los documentos que le presenten para la extensión o autorización de instrumentos públicos notariales protocolares y extraprotocolares” .

" q) Brindar las medidas de accesibilidad necesarias, los ajustes razonables y salvaguardias que la persona requiera."(*)

(*) Literal incorporado por el Artículo 8 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.
