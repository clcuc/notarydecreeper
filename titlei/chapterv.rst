######################################
CAPÍTULO V DE LOS DERECHOS DEL NOTARIO
######################################

Artículo 19.- Derechos del Notario
##################################

Son derechos del notario:

a) La inamovilidad en el ejercicio de su función.

b) Ser incorporado en la planilla de su oficio notarial,con una remuneración no mayor al doble del trabajador mejor pagado(*) , y los derechos derivados propios del régimen laboral de la actividad privada.

(*) De conformidad con el Resolutivo 2 de la Sentencia de Expedientes  N° 0009, 00015 y 00029-2009-PI-TC , publicada el 30 septiembre 2010, se declara inconstitucionalel extremo cuestionado del inciso b) del presente artículo.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 12

c) Gozar de vacaciones, licencias por enfermedad, asistencia a certámenes nacionales o internacionales y razones debidamente justificadas.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 13

d) Negarse a extender instrumentos públicos contrarios a la ley, a la moral o a las buenas costumbres; cuando se le cause agravio personal o profesional y abstenerse de emitir traslados de instrumentos autorizados cuando no se le sufrague los honorarios profesionales y gastos en la oportunidad y forma convenidos.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 14

e) El reconocimiento y respeto de las autoridades por la importante función que cumple en la sociedad, quienes deberán brindarle prioritariamente las facilidades para el ejercicio de su función; y,

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 3

f) El acceso a la información con que cuenten las entidades de la administración pública y que sean requeridos para el adecuado cumplimiento de su función, salvo las excepciones que señala la ley.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 3

Artículo 20.- Encargo del Oficio Notarial
#########################################

En caso de vacaciones o licencia, el colegio de notarios, a solicitud del interesado, designará otro notario de la misma provincia para que se encargue del oficio del titular. Para estos efectos, el colegio de notarios designará al notario propuesto por el notario a reemplazar.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 13
