################################
CAPÍTULO VI DEL CESE DEL NOTARIO
################################

Artículo 21.- Motivos de Cese
#############################

El notario cesa por:

a) Muerte.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 1

b) Al cumplir setenta y cinco (75) años de edad.(*)

(*) Inciso declarado inconstitucional por el Resolutivo 2 de la Sentencia   de Expedientes N° 0009, 00015 y 00029-2009-PI-TC , publicada el 30 septiembre 2010.

c) Renuncia.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 1

d) Haber sido condenado por delito doloso mediante sentencia firme.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 1

e) No incorporarse al colegio de notarios por causa imputable a él, dentro del plazo establecido por el artículo 13 de la presente ley.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 1

f) Abandono del cargo, por no haber iniciado sus funciones dentro del plazo a que se refiere el artículo 15 de la presente ley, declarada por la junta directiva del colegio respectivo.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 2

g) Abandono del cargo en caso de ser notario en ejercicio, por un plazo de treinta (30) días calendario de inasistencia injustificada al oficio notarial, declarada por la junta directiva del colegio respectivo.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 2

h) Sanción de destitución impuesta en procedimiento disciplinario.

i) Perder alguna de las calidades señaladas en el artículo 10 de la presente ley, declarada por la Junta Directiva del colegio respectivo, dentro de los sesenta (60) días calendario siguientes de conocida la causal.

CONCORDANCIAS:       D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 2 y Art. 16, num. 4

j) Negarse a cumplir con el requerimiento del Consejo del Notariado a fin de acreditar su capacidad física y/o mental ante la institución pública que éste designe. Esta causal será declarada mediante Resolución del Consejo del Notariado, contra la cual procede recurso de reconsideración; y,

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num . 3

k) Inhabilitación para el ejercicio de la función pública impuesta por el Congreso de la República de conformidad con los artículos 99 y 100 de la Constitución Política.

En el caso de los incisos a), b), c), d) y e) el colegio de notarios comunicará que ha operado la causal de cese al Consejo del Notariado, para la expedición de la resolución ministerial de cancelación de título.

En el caso de los incisos f) g), h), i) y j) el cese se produce desde el momento en que quede firme la resolución. Para el caso del inciso k) el cese surte efectos desde el día siguiente a la publicación de la resolución legislativa en el diario oficial El Peruano.

En caso de cese de un notario en ejercicio, el colegio de notarios, con conocimiento del Consejo del Notariado, se encargará del cierre de sus registros, sentándose a continuación del último instrumento público de cada registro, un acta suscrita por el Decano del colegio de notarios donde pertenezca el notario cesado.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

“ Artículo 21.- Motivos de cese

El notario cesa por:

a) Muerte.

b) Renuncia.

c) Haber sido condenado por delito doloso mediante sentencia firme, independientemente de la naturaleza del fallo o la clase de pena que haya impuesto el órgano jurisdiccional.

d) No incorporarse al colegio de notarios por causa imputable a él, dentro del plazo establecido por el artículo 13 de la presente ley.

e) Abandono del cargo, por no haber iniciado sus funciones dentro del plazo a que se refiere el artículo 15 de la presente ley, declarada por la junta directiva del colegio respectivo.

f) Abandono del cargo en caso de ser notario en ejercicio, por un plazo de treinta (30) días calendario de inasistencia injustificada al oficio notarial, declarada por la junta directiva del colegio respectivo.

g) Sanción de destitución impuesta en procedimiento disciplinario.

h) Perder alguna de las calidades señaladas en el artículo 10 de la presente ley, declarada por la Junta Directiva del colegio respectivo, dentro de los sesenta (60) días calendario siguientes de conocida la causal.

i) Negarse a cumplir con el requerimiento del Consejo del Notariado a fin de acreditar su capacidad física y/o mental ante la institución pública que éste designe. Esta causal será declarada mediante Resolución del Consejo del Notariado, contra la cual procede recurso de reconsideración.

j) Inhabilitación para el ejercicio de la función pública impuesta por el Congreso de la República de conformidad con los artículos 99 y 100 de la Constitución Política” .

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 15, num. 4

“ Artículo 21-A.- Procedimiento en caso de cese

En el caso de los literales a), b), c) y d) del artículo 21, el colegio de notarios comunicará que ha operado la causal de cese al Consejo del Notariado, para la expedición de la resolución ministerial de cancelación de título.

En el caso de los literales e), f) g), h) e i) el cese se produce desde el momento en que quede firme la resolución. Para el caso del literal j) el cese surte efectos desde el día siguiente a la publicación de la resolución legislativa en el diario oficial El Peruano.

En caso de cese de un notario en ejercicio, el Colegio de Notarios en el plazo de treinta (30) días, se encargará del cierre de sus registros, de solicitar la cancelación del título, de nombrar al notario administrador del acervo y de comunicar al Consejo del Notariado. Para ello, se asienta a continuación del último instrumento público de cada registro, un acta suscrita por el Decano del colegio de notarios donde pertenezca el notario cesado.

En caso de incumplimiento, el Consejo del Notariado requerirá al Colegio de Notarios para que en el plazo de treinta (30) días cumpla con lo dispuesto en el párrafo precedente, luego de los cuales asumirá funciones el Consejo del Notariado, bajo responsabilidad de la Junta Directiva del Colegio de Notarios.

Asimismo, luego de transcurridos dos (02) años del cese, el colegio de notarios entregará al Archivo General de la Nación el acervo documentario del notario cesado” .(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1232, publicado el 26 septiembre 2015.

Artículo 22.- Medida Cautelar
#############################

Ante indicios razonables que hagan prever el cese del notario por pérdida de calidades señaladas para el ejercicio del cargo, de acuerdo a lo señalado en el artículo 10 de la presente ley y en tanto se lleva adelante el procedimiento señalado en el artículo 21 inciso i) precedente, el Consejo del Notariado mediante decisión motivada podrá imponer la medida cautelar de suspensión del notario. Procede recurso de reconsideración contra dicha resolución, el mismo no suspende la ejecución de la medida cautelar.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 16
