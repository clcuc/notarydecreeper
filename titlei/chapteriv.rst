###########################################
CAPÍTULO IV DE LAS PROHIBICIONES AL NOTARIO
###########################################

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 11

Artículo 17.- Prohibiciones al Notario
######################################

Está prohibido al notario:

a) Autorizar instrumentos públicos en los que se concedan derechos o impongan obligaciones a él, su cónyuge, ascendientes, descendientes o parientes consanguíneos o afines dentro del cuarto y segundo grado, respectivamente.

b) Autorizar instrumentos públicos de personas jurídicas en las que él, su cónyuge, o los parientes indicados en el inciso anterior participen en el capital o patrimonio, salvo en aquellos casos de sociedades que se cotizan en la bolsa de valores; así como de aquellas personas jurídicas en las que tengan la calidad de administradores, director, gerente, apoderados o representación alguna.

c) Ser administrador, director, gerente, apoderado o tener representación de personas jurídicas de derecho privado o público en las que el Estado, gobiernos regionales o locales, tengan participación.

d) Desempeñar labores o cargos dentro de la organización de los poderes públicos y del gobierno nacional, regional o local; con excepción de aquellos para los cuales ha sido elegido mediante consulta popular o ejercer el cargo de ministro y viceministro de Estado, en cuyos casos deberá solicitar la licencia correspondiente. También podrá ejercer la docencia a tiempo parcial y desempeñar las labores o los cargos otorgados en su condición de notario. Asimismo, podrá ejercer los cargos públicos de regidor y consejero regional sin necesidad de solicitar licencia.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art.11, num. 5

e) El ejercicio de la abogacía, excepto en causa propia, de su cónyuge o de los parientes indicados en el inciso a) del presente artículo.

f) Tener más de una oficina notarial.

CONCORDANCIAS:      D.S. Nº 010-2010-JUS (TUO del Reglamento), Art. 11, num. 2

g) Ejercer la función fuera de los límites de la provincia para la cual ha sido nombrado, con excepción de lo dispuesto en el inciso k) del artículo 130 de la presente ley y el artículo 29 de la Ley Nº 26662; y,

h) El uso de publicidad que contravenga lo dispuesto en el Código de Ética del notariado peruano.

CONCORDANCIAS:      D.S.Nº 010-2010-JUS (TUO del Reglamento), Art. 11. num. 3

i) La delegación parcial o total de sus funciones

Artículo 18.- Prohibición de Asumir Funciones de Letrado
########################################################

Se prohíbe al notario autorizar minuta, salvo el caso a que se refiere el inciso e) del artículo que precede; la autorización estará a cargo de abogado, con expresa mención de su número de colegiación.

No está prohibido al notario, en su calidad de letrado, el autorizar recursos de impugnación que la ley y reglamentos registrales franquean en caso de denegatoria de inscripción.
